/**
 * UserInfoController
 *
 * @description :: Server-side logic for managing Userinfoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var constants = require('../../constants.js'),
	Q = require('q'),
	validator = require('validator');

module.exports = {

	edit: function(req, res, next) {
		Q.all([
			//TODO Remove user 7 by req.user.id!!
			User.findOne({ id: 7 }).populate( 'sports' ),
			Sport.find()
		])
		.spread(function( userSports, sports ){
			//var userPrefSports = [], auxSport, sportsList;
			return sports.map(function( sport, index, array ){
				pos = userSports.sports.map(function( uSport ) { return uSport.id; }).indexOf( sport.id );
				if( pos > -1 ) {
					sport.userPref = true;
				}
				//Filter the sport element
				//auxSport = SportService.filterSport( sport, req.userRequestLanguage );
				//if( auxSport.userPref ) userPrefSports.push( auxSport );
				return SportService.filterSport( sport, req.userRequestLanguage );
			});
			//return sportsList;
		})
		.then(function( sportsList ){

			return res.format({
				html: function(){
					res.view({
						layout: 'baseLayout.ejs',
						baseLayout: 'userInterests/baseEditLayout.ejs',
						sports: sportsList,
						authUser: req.user
					});
				},
				json: function(){
					res.json({ 
						sports: sportsList
					});
				}
			});
		})
		.fail(function( err ){
			sails.log.error('UserInterestsController edit|' + err + '|idUser:' + req.param('id'));
			return res.format({
				html: function(){
		 			res.serverError()
				},
				json: function(){
					return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' )}, 500);				
				}
			});		
		});
	},	

	create: function(req, res, next){
		var idSport = req.param('id_sport'),
			idUser = req.param('id_user');
		if( !validator.isInt( idSport ) || !validator.isInt( idSport ) ) {
    		return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}

		Sport.findOne({ id: idSport })
			.then(function( sport ){
				var deferred = Q.defer();
				sport.owners.add( idUser );
				sport.save(function( err ){
					if( err ){
						deferred.reject( new Error( err ) );  		
				    	return;					
					}
					deferred.resolve();					
				});
				return deferred.promise;
			})
			.then(function(){
				res.json(200);
			})
			.fail(function(err){
				sails.log.error('UserInterestsController create|' + err + '|idSport:' + idSport + '|idUser:' + idUser);			
				return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
			});
	},

	delete: function(req, res, next){
		var idSport = req.param('id_sport'),
			idUser = req.param('id_user');
		if( !validator.isInt( idSport ) || !validator.isInt( idSport ) ) {
    		return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}

		Sport.findOne({ id: idSport })
			.then(function( sport ){
				var deferred = Q.defer();
				sport.owners.remove( idUser );
				sport.save(function( err ){
					if( err ){
						deferred.reject( new Error( err ) );  		
				    	return;					
					}
					deferred.resolve();					
				});
				return deferred.promise;
			})
			.then(function(){
				res.json(200);
			})
			.fail(function(err){
				sails.log.error('UserInterestsController create|' + err + '|idSport:' + idSport + '|idUser:' + idUser);			
				return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
			});
	},		
};

