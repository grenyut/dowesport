/**
 * AuthController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

var passport = require('passport'),
    utils = require('../../utils.js');


var AuthController = {

    //TODO Test logout
    logout: function (req, res) {
        res.clearCookie('remember_me');
        // The user is "logging out" (e.g. destroying the session) so change the online attribute to false.
        User.update(req.user.id, {
            online: false
        }).then(function( user ){
            // Wipe out the session (log out)
            req.logout();

            // Redirect the browser to the sign-in screen
            res.redirect('/');
        }).fail(function( err ){
            sails.log.error('authController logout|' + err.message + '|id: ' + req.user.id);
            req.logout();
            res.redirect('/');
        });
    },

    login: function(req, res, next) {
        var redirectUrl;
        passport.authenticate( 'local', function( err, user ) {
            if ( err ) {
                sails.log.error(err);
                return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
            }
            // If no user is found...
            if ( !user ) {
                return res.json({ msg: res.i18n( 'ERR_NO_EMAIL' ) }, 400);
            }

            // If the account has not been verified
            if ( !user.active ) {
                return res.json({ msg: res.i18n( 'ERR_NO_ACTIVE_ACCOUNT' ) }, 400);
            }

            if( req.session.lastPage ) {
                redirectUrl = req.session.lastPage;
            } else {
                redirectUrl = '/';
            }      

            if ( req.body.remember_me ) { 
                console.log('---->remember_me set');
                var token = utils.randomString(64);

                Token.create({ remember_me_token: token, id_user: user.id } , function userUpdated(err) {
                    console.log('---->remember_me updated? err:');
                    console.log(err);

                    if (err) return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);

                    res.cookie('remember_me', token, { path: '/', httpOnly: true, maxAge: 604800000 }); // 7 days
                        
                    req.logIn(user, function (err) {
                        if (err) {
                            sails.log.error(err);
                            return res.json({ msg: res.i18n( 'ERR_NO_SESSION_CREATED' ) }, 401);
                        }
                        return res.json({ redirectUrl: redirectUrl }, 200);
                    }); 
                });     

            } else {
                // req.user = user;
                // // Change status to online
                // user.online = true;  
                req.logIn(user, function (err) {
                    if (err) {
                        sails.log.error(err);                        
                        return res.json({ msg: res.i18n( 'ERR_NO_SESSION_CREATED' ) }, 401);
                    }
                    //Redirect to their profile page (e.g. /views/user/show.ejs)
                    //res.send({ url: '/user/show/' + user.id });
                    return res.json({ redirectUrl: redirectUrl }, 200);
                }); 
            }
        }) (req, res);
    },    

    //TODO check this failure redirections!!!

    'facebook': function (req, res) {
        passport.authenticate('facebook', { failureRedirect: '/' },
            function (err, user) {
                var redirectUrl;
                if( req.session.lastPage ) {
                    redirectUrl = req.session.lastPage;
                } else {
                    redirectUrl = '/';
                }                   
                req.logIn(user, function (err) {
                    if (err) {
                        console.log(err);
                        //res.view('500');
                        return res.json({ msg: err }, 500);
                    }
                    console.log('facebook login:' );
                    //console.log(req.user);
                    return res.json({ redirectUrl: req.session.lastPage }, 200);
                });
            })(req, res);
    },

    'facebook/callback': function (req, res) {
        passport.authenticate('facebook',
            function (req, res) {
                var redirectUrl;
                if( req.session.lastPage ) {
                    redirectUrl = req.session.lastPage;
                } else {
                    redirectUrl = '/';
                }                 
                res.json({ redirectUrl: req.session.lastPage }, 200);
            })(req, res);
    },

    'google': function (req, res) {
        passport.authenticate('google', { failureRedirect: '/', scope:['https://www.googleapis.com/auth/plus.login','https://www.googleapis.com/auth/userinfo.profile'] },
            function (err, user) {
                var redirectUrl;
                if( req.session.lastPage ) {
                    redirectUrl = req.session.lastPage;
                } else {
                    redirectUrl = '/';
                }                 
                req.logIn(user, function (err) {
                    if (err) {
                        console.log(err);
                        //res.view('500');
                        return res.json({ msg: err }, 500);
                    }
                    return res.json({ redirectUrl: req.session.lastPage }, 200);
                });
            })(req, res);
    },

    'google/callback': function (req, res) {
        passport.authenticate('google',
            function (req, res) {
                var redirectUrl;
                if( req.session.lastPage ) {
                    redirectUrl = req.session.lastPage;
                } else {
                    redirectUrl = '/';
                }                 
                res.json({ redirectUrl: req.session.lastPage }, 200);
            })(req, res);
    }

};
module.exports = AuthController;