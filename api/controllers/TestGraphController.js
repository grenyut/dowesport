/**
 * TestGraphController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    

	testInsertNode: function(req, res, next) {
  		var user = {email:req.param('email')};
  		console.log(user);
		UserService.createPersonNode(user);	
	},

	testGetNodes: function(req, res, next) {
  		var user = {email:req.param('email')};
		UserService.getKnownEmails(user, function(err, results) {
			res.send(results);
		});	
	},	

	testSetRelationship: function(req, res, next) {
  		var user = {email: 'marc'};

		UserService.setKnowRelationship(user, req.param('destemail'),function(err, results) {
			res.send(results);
		});	
	},		


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to TestGraphController)
   */
  _config: {}

  
};
