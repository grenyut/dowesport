/**
 * ImageController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

 var constants = require('../../constants.js'),
    Q = require('q'),
    validator = require('validator');

module.exports = {
    
  	profile: function profile(req, res, next) {
  		User.update(req.param('id'), {avatar: req.user.avatar}, function userUpdated(err, users) {
  			if (err) {
          sails.log.error(err);
          res.json({ msg: res.i18n('ERR_UNEXPECTED') }, 500);
        }
  			setTimeout(function(){
            res.json( { imgUrl: req.user.avatar }, 200);
    			}, 3000);
  		});
    },

    uploadEventImage: function uploadEventImage(req, res, next) {
        res.json( req.mediumImageDestPath, 200);
    },    

    deleteEventImage: function deleteEventImage(req, res, next) {
        console.log('deleteEventImage');
        var eventId = req.param('id'),
            imageId = req.param('id_image');

        if( !validator.isInt( eventId ) || !validator.isInt( imageId ) ){
            return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);            
        }
        var dirPath = [ ImageService.eventDirPath( eventId ), '/', imageId ],        
            largeImagePath = dirPath.join('') + constants.bigImgExtension + constants.jpegExtension,
            mediumImagePath = dirPath.join('') +  constants.mediumImgExtension + constants.jpegExtension;

        
        console.log(req.body);
        console.log(largeImagePath);
        console.log(mediumImagePath);


        Q.all([
            ImageService.unlink( largeImagePath ),
            ImageService.unlink( mediumImagePath )
        ])                
        .then(function(){
            res.json( { idImg: imageId }, 200);
        })
        .fail(function( err ){
            sails.log.error('ImageController deleteEventImage|' + err + '|idEvent:' + eventId);
            res.json({ msg: res.i18n('ERR_UNEXPECTED') }, 500);
        });
    }, 

    getImage: function getImage(req, res, next) {
        res.sendfile( 'assets/public/' + req.path.substr(1) );
    },      


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ImageController)
   */
  _config: {
    rest: false,
    shortcuts: false
  }

  
};
