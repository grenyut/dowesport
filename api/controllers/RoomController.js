/** * UserController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var constants = require('../../constants.js');

 module.exports = {


	/** 
	*	show the invitation with the event and the assistants
	*	Restriction: the user must be a participatant
	**/
	show: function(req, res, next) {
		console.log('roomController show params:');
		console.log('id_event: ' + req.param('id'));

		Event.findOne({
			id: req.param('id')
		}).then(function( event ){
			if( event ) {
				res.view({
					authUser: req.user,
					event: event,
					layout: 'baseLayout.ejs',
					baseLayout: 'invitation/baseShowLayout.ejs'			
				});
			} else {
				var errMsg = res.i18n( 'ERR_NO_EVENT_EXISTS' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);				
			}
		}).fail(function(err) {
			sails.log.error(err.message + '|roomController show id_event: ' + req.param('id'));			
			res.redirect('/');
		});
	},		

	'new': function(req, res) {
		res.view();
	},
	
	create: function(req, res, next) {

	},

	index: function(req, res, next) {

	},	

	edit: function(req, res, next) {

	},

	update: function(req, res, next) {

	},

   	/**
	/ Destroys a user from the database
	/ Still not giving the option to the user
	/ We probably should not remove the user from the db
	**/
	destroy: function(req, res, next) {

	},	

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
   _config: {}


};
