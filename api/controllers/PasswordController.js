/**
 * PasswordController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var utils = require('../../utils.js');

module.exports = {

	/**
	/ Sends an email to recover a password
	**/
	forgotPassword: function(req, res, next) {
		User.findOne({
			email: req.body.email,
		}, 
		function foundUser(err, user) {
			var resetPasswordToken;
			if (err) {
				sails.log.error(err);
				return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
			}
			if (!user) {
				return res.json({ msg: res.i18n( 'ERR_NO_USER_EMAIL' ).replace( '##email##', req.body.email ) }, 400);
			} 

			resetPasswordToken = utils.randomString(100);
			User.update(
				{ email: req.body.email },
				{ reset_password_token: resetPasswordToken },
				function( err, users ) {
					if ( err ) {
						sails.log.error(err);
						req.session.flash = { err: { msg: res.i18n( 'ERR_UNEXPECTED' ) } };
					}
					user.reset_password_token = resetPasswordToken;
					//Sending email forgot password
					EmailService.dispatchForgotPasswordEmail(res, user, function(err) {
						if (err) sails.log.error(err);
					});	

					req.session.flash = { message: { msg: res.i18n( 'emailForgotPasswordSend' ).replace( '##email##', user.email ) } };
					return res.json({ redirectUrl: '/' }, 200);
				});
		});
	},

	/**
	/ If the reset_password_token exists, the app shows the form to 
	/ enter a new password
	**/
	resetPassword: function(req, res, next) {
		//TODO Test reset password
		User.findOne({
			reset_password_token: req.param('secret')
		}).then(function( user ){
			if (!user) {
				var errMsg = res.i18n( 'ERR_NO_USER' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error( errMsg );			
			}
			//Log user in
			req.login(user, function(err) {
				if (err) {
                    sails.log.error(err);
					req.session.flash = { err: { msg: res.i18n( 'ERR_NO_SESSION_CREATED' ) } };
                    return res.redirect('/');
				}
				res.view( 'password/edit', { id: user.id } );
			});				
        }).fail(function( err ){
            sails.log.error('authController resetPassword|' + err.message + '|secret: ' + req.param('secret'));
            req.logout();
            res.redirect('/');
        });
	},

	//TODO test new password
	newPassword: function(req, res, next) {
		console.log('------->newPassword value: ' + req.param( 'password' ));
		console.log('------->reset_password_token value: ' + req.param( 'reset_password_token' ));
		User.update({
			email: req.user.email 
		}, { 
			password: req.param( 'password' ),
			reset_password_token: req.param( 'reset_password_token' )
		})
		.then( function( users ){
			res.redirect('/');	
		})
		.fail(function( err ){
            sails.log.error('authController newPassword|' + err.message + '|email: ' + req.user.email);            
			req.session.flash = { err: { msg: res.i18n( 'ERR_NO_OPERATION_SUCCESS' ) } };
            return res.redirect('/');					
		});
	},	


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to PasswordController)
   */
  _config: {}

  
};


