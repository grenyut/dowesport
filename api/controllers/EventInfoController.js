/**
 * EventInfoController
 *
 * @description :: Server-side logic for managing event additional info like attending users, comments, etc
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var constants = require('../../constants.js'),
	Q = require('q');

module.exports = {

	eventComments: function(req, res, next) {
 		EventComment.find()
 			.where({ id_event: req.event.id })
 			.populate('user')
 			.then(function( comments ){
 				comments = comments.map(function( comment, index, array ){
					return comment.toJSON();
 				})
 				res.json( comments );
 				return;
 			})
			.fail(function( err ){
				sails.log.error('EventInfoController eventComments|' + err + '|idEvent:' + req.param('id'));
				return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
			}).done();
	},	

	eventCreateComment: function(req, res, next){
		var comment = {
			id_event: req.event.id,
			message: req.param('message'),
			user: req.user.id
		};
		Q.all([
			EventComment.create( comment ).then(function( comment ) {
				return comment;
			}),
			User.findOne({ id: req.user.id }).then(function( user ){
				return user;
			})
		]).spread(function( comment, user ){
			comment.user = user;
			res.json( comment );
		}).fail(function( err ) {
			sails.log.error('EventInfoController eventCreateComment|' + err + '|idEvent:' + req.param('id') + '|idUser:' + req.user.id);
			return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
		}).done();
	},


	//Method not being used!
	// eventUsers: function(req, res, next) {
	// 	console.log('eventUserssss');
	// 	Event.findOne()
	// 	.where({ id: req.param('id') })
	// 	.then(function( event ){
	// 	 	if( !event ) throw new Error( 'event ' + req.param('id') + ' not found');
	// 	 	req.event = event;
	// 	 	//if private event look for an invitation
	// 	 	if( constants.privateEvent == event.is_public && req.user) {
	// 	 		return Invitation.findOne()
	// 	 			.where({ 
	// 	 				email: req.user.email,
	// 	 				id_event: event.id
	// 	 			});
	// 	 	} else if( constants.privateEvent == event.is_public && !req.user) {
	// 	 		throw new Error( 'NOT_INVITED' );
	// 	 	} else return true;
	// 	})
	// 	.then(function( invitation ){
	// 	 	if( invitation && invitation.status == constants.invitationNoResponse ) {
	// 			throw new Error( 'NOT_ACCEPTED' ); 	
	// 	 	} else if( req.event.is_public == constants.publicEvent || ( invitation && invitation.status == constants.invitationAccepted )) {
	// 	 		arrayQuery = [
	// 	 			req.event.id,
	// 	 			constants.invitationNoResponse,
	// 	 			constants.invitationAccepted
	// 	 		]; 
	// 	 		return EventService.getEventGuests( arrayQuery );
	// 	 	} else {
	// 	 		throw new Error( 'NOT_INVITED' );
	// 	 	}
	// 	})
	// 	.then(function( users ){
	// 		console.log('eventUsers');
	// 	 	console.log(users);
	// 		res.json( users );
	// 		return;		 	
	// 	})
	// 	.fail(function( err ){
	// 		sails.log.error('EventInfoController eventUsers|' + err + '|idEvent:' + req.param('id'));
	// 	}).done();
	// },	
};

