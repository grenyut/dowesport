/** * UserController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var constants = require('../../constants.js');

 module.exports = {

	// This loads the sign-up page --> new.ejs //Should not be used but facebook and google error
	// redirections still use it
	// 'new': function(req, res) {
	// 	res.view();
	// },
	
	/**
	/ create a user (pocicy createUser) and send verification email
	**/
	create: function(req, res, next) {
		var user = req.user, redirectUrl;

		if( UserService.validateUserCoords( req.body ) ){
			return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}

		//Create Person node into the graph database
		//TODO Uncomment
		//UserService.createPersonNode(user);
	    var userObj = {
	        first_name: req.param('name'),
	        email: req.param('email'),
	        password: req.param('password'),
	        confirmation: req.param('confirmation'),
	        country: req.param('localization_country'),
	        city: req.param('localization_city'),
	        online: true
	    }

	    // Create a User with the params sent from 
	    // the sign-up form --> new.ejs
	    User.create(userObj, function userCreated(err, user) {
	        if ( err ) {
	            sails.log.error( err );
	            if(err.code === 'ER_DUP_ENTRY') {
	                return res.json({ msg: res.i18n( 'ERR_ALREADY_USER_EXISTS' ) }, 404);
	            } else {
	                return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);	                
	            }
	        } else {
				//send verification email
				EmailService.dispatchNewRegistrationEmail(res, user, function(err) {
					if (err) sails.log.error(err);
				});			

				if( req.session.lastPage ) {
					redirectUrl = req.session.lastPage;
				} else {
					redirectUrl = '/';
				}

				//Update user location coordinates
				var arrayQuery = [
					'POINT(' + req.param('localization_lat') + ' ' + req.param('localization_lon') + ')',
					user.id
				]; 

				User.query("UPDATE " + constants.dbName + ".users SET \
					city_point = GeomFromText(?) \
					WHERE id = ?", arrayQuery, 
					function( err, result ) {
				    	if( err ){
				    		res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
				    		return;
				    	}

				    	user.city_point = {
				    		lat: req.param('localization_lat'),
				    		lon: req.param('localization_lon')
				    	};
						//Log user in
						req.login( user, function( err ) {
							if( err ) {
			                    sails.log.error(err);
		                        return res.json({ msg: res.i18n( 'ERR_NO_SESSION_CREATED' ) }, 401);
							}
							return res.json({ redirectUrl: redirectUrl }, 200);	
						});		
					});
	        } 
	    });
	},

	// render the profile view (e.g. /views/show.ejs)
	show: function(req, res, next) {
		var img_size;
		User.findOne({
			id: req.param('id'),
			active: true
		})
		.populate('sports')
		.exec(function foundUser(err, user) {
			img_size = ImageService.imageSize(req.param('img_size'));
			if (err) {
				sails.log.error('UserController show|' + err + '|idUser:' + req.param('id'));
				return res.serverError();
			}
			if (!user) return res.notFound();

			return res.format({
				html: function(){
					res.view({
						layout: 'baseLayout.ejs',
						baseLayout: 'user/baseShowLayout.ejs',
						user: user,
						authUser: req.user
					});
				},
				json: function(){
					res.json({ user: user, });
				}
			});

		});
	},	

	/**
	/ Only for admin users
	/ Shows a list of users to edit  and see their details
	**/	
	index: function(req, res, next) {
		// Get an array of all users in the User collection(e.g. table)
		User.find(function foundUsers(err, users) {
			if (err) return next(err);
			// pass the array down to the /views/index.ejs page
			res.view({
				users: users,
				authUser: req.user
			});
		});
	},	

	// render the edit view (e.g. /views/edit.ejs)
	edit: function(req, res, next) {
		var img_size, birthdate = '';
		// Find the user from the id passed in via params
		User.findOne(req.param('id'), function foundUser(err, user) {
			if (err) return next(err);
			if (!user) return next('User doesn\'t exist.');
			if( user.birthdate ) {
				birthdate = user.birthdate.split('-');
			}
			if( birthdate.length === 3 )
				user.birthdate = birthdate[2] + '/' + birthdate[1] + '/' + birthdate[0];
			res.view(
				'user/edit', 
				{
					user: user,
					authUser: req.user,
					layout: 'baseLayout.ejs', 
					baseLayout: 'user/baseEditLayout.ejs'
				}
			);				

		});
	},

	// process the info from edit view
	update: function(req, res, next) {
		var birthdate;
		User.findOne(req.param('id'), function foundUser(err, user) {
			if (err) return next(err);
			if (!user) return next('User doesn\'t exist.');

			if ( req.param('birthdate') ) {
				birthdate = req.param('birthdate').split('/');
				if( birthdate.length === 3 )
					user.birthdate = birthdate[2] + '-' + birthdate[1] + '-' + birthdate[0];
			}

			user.first_name = req.param('name');
			user.email = req.param('email');
			user.biography = req.param('bio');
			user.gender = req.param('gender');
			user.country = req.param('country');
			user.city = req.param('city');

			if (req.user.admin)	user.admin = req.param('admin');

			User.update(req.param('id'), user, function userUpdated(err, users) {
				if (err) {
					return res.redirect('/user/edit/' + req.param('id'));
				}

				res.redirect('/user/show/' + req.param('id'));
			});
		});
	},

	/**
	/ Verify a user account by the email (e) and the hash (h) params
	**/
	//TODO test verify function
  //   verify: function (req, res, next) {
		// User.update({ 
		// 	email: req.param('e'),
		// 	active_token: req.param('h')
		// }, { 
		// 	active: true 
		// }).then(function( users ){
		// 	//Log user in
		// 	req.login(users[0], function(err) {
		// 		if (err) { 
		// 			sails.log.error('userController verify|' + err.message + '|email: ' + req.param('e') + '|token:' + req.param('h'));			
		// 			req.session.flash = { message: { msg: res.i18n( 'ERR_UNEXPECTED' ) } };											
		// 		}

		// 		// Let other subscribed sockets know that the user was created.
		// 		//User.publishCreate(user);
		// 		return res.redirect( '/' );
		// 	});											
		// }).fail(function( err ){
		// 	sails.log.error('userController verify|' + err.message + '|email: ' + req.param('e') + '|token:' + req.param('h'));			
		// 	return res.redirect('/');
		// });
  //   },	

    removeaccount: function (req, res, next) {
		User.update({ 
			email: req.param('e'),
			active_token: req.param('h'),
			active: true
		}, { 
			active: false 
		}).then(function( users ){
			//Log user in
			req.login(users[0], function(err) {
				if (err) { 
					sails.log.error('userController verify|' + err.message + '|email: ' + req.param('e') + '|token:' + req.param('h'));			
					req.session.flash = { message: { msg: res.i18n( 'ERR_UNEXPECTED' ) } };											
				}

				// Let other subscribed sockets know that the user was created.
				//User.publishCreate(user);
				req.session.flash = { message: { msg: res.i18n( 'userDeleteSuccess' ) } };		
				return res.redirect( '/' );
			});											
		}).fail(function( err ){
			sails.log.error('userController verify|' + err.message + '|email: ' + req.param('e') + '|token:' + req.param('h'));			
			return res.redirect('/');
		});
    },	    

   	/**
	/ Destroys a user from the database
	/ Still not giving the option to the user
	/ We probably should not remove the user from the db
	**/
	//TODO test destroy user method
	destroy: function(req, res, next) {
		User.destroy({
			id: req.param('id')
		}).exec(function(err) {
			// Error handling
			if (err) {
				req.session.flash = { message: { msg: res.i18n( 'ERR_UNEXPECTED' ) } };															
				sails.log.error('userController destroy|' + err.message + '|id_user: ' + req.param('id'));			
			} else {
				req.session.flash = { message: { msg: res.i18n( 'userDeleteSuccess' ) } };															
			}
			res.redirect('/');
		});
	},	

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
   _config: {}


};
