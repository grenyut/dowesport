var constants = require('../../constants.js'),
	Q = require('q');

module.exports = {

	new: function(req, res) {
		res.view({
			event: {},
			authUser: req.user,
			date: new Date().toDateString(),
			layout: 'baseLayout.ejs',
			baseLayout: 'event/baseNewLayout.ejs',
			is_public: constants.publicEvent,
			is_private: constants.privateEvent
		});
	},

	create: function(req, res, next) {
		var newEventImagePath, eventDate, loc, queryStringArray, arrayQuery;

		// Param validation
		if( !EventService.validateEventStep1(req.body) ) {
    		return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}

		//TODO ERASE
		sails.log.info('ERASE user.id assignation');
		req.user = UserService.getUserFromRequest(req);

		//Create event with this information
		loc = LocalizationService.formatLocalization( req.param('localization') );
		eventDate = DateService.formatDate( req.param('initDate') );

		arrayQuery = [
			req.user.id,
			req.param('title'),
			req.param('description'),
			req.param('activity'),
			1, //status
			req.param('publicEvent'),
			eventDate,
			EventService.formatIdTitle( req.param('title') ),
			loc.city,
			loc.country,
			loc.name,
			loc.formatted_address,
			'POINT(' + loc.lat + ' ' + loc.lon + ')'
		];

		queryStringArray = ["INSERT INTO " + constants.dbName + ".events SET ",
						"id_owner = ?, ",
						"title = ?, ",
						"description = ?, ",
						"id_sport = ?, ",
						"id_status = ?, ",
						"is_public = ?, ",
						"date_init = ?, ",
						"id_title = ?, ",
						"localization_city = ?, ",
						"localization_country = ?, ",
						"localization_name = ?, ",
						"localization_formatted_address = ?, ",
						"localization_point = GeomFromText(?)"];

		if( req.place ) {
			arrayQuery.push( req.place.id );
			queryStringArray.push(', id_place = ?');
		}

		var query = Event.query( queryStringArray.join(''), arrayQuery, 
			function(err, result) {
				var newInvitation;

		    	if( err ){
		    		sails.log.error(err);	    		
	            	return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
		    	}

				newEventImagePath = ImageService.eventDirPath( result.insertId );

				newInvitation = {
					email: req.user.email,
					id_event: result.insertId,
					status: constants.invitationAccepted
				};

				Q.all([
					ImageService.makeDir( newEventImagePath ),
					Invitation.create( newInvitation )
						.then(function( invitation ){
							console.log('INVITATION CREATED!')
							return invitation;
						})
						.fail(function(err){
							throw new Error( err );
						})
				])
				.spread( function( finalEvent, invitation ){
					// if( req.param('status') == constants.eventPublished){
					// 	return res.json({ redirect: URLService.getCreateInvitationLink( result.insertId )});
					// } 
					return res.json({ redirect: URLService.getEventEditLink( result.insertId )});
				})					
				.fail( function(err){
		    		sails.log.error(err);	    		
	            	return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
				});				

			});
	},

	//Tested
	show: function(req, res, next) {
		var eventiDirPath  = ImageService.eventDirPath( req.param('id') ),
			arrayQuery = [
				req.event.id,
				constants.invitationNoResponse,
				constants.invitationAccepted
			];
		//Find event and its images
		Q.all([
			ImageService.readDir( eventiDirPath )
				.then(function( files ){
					return files;
				}),
			User.findOne().where({ id: req.event.id_owner }).then(function( owner ){
				return owner;
			}), 
			EventService.getEventGuests( arrayQuery )
		])
		.spread(function( files, owner, guests ){
			if (!owner) throw new Error('user ' + req.event.id_owner + ' not found for event ' + req.param('id'));
			
			var files = ImageService.getSpecificFiles( files, constants.mediumImgExtension )
				.map(function getEventImageDirPath( element, index, array ) {
					return ImageService.relativeEventDirPath( req.param('id') ) + '/' + element;
				});				

			req.event.files = files;
			res.format({
				html: function(){
					res.view({
						layout: 'baseLayout.ejs',
						baseLayout: 'event/baseShowLayout.ejs',
						authUser: req.user,
						owner: owner,
						event: req.event,
						guests: guests
					});
				},
				json: function(){
					res.json({
						owner: owner,
						event: req.event,
						guests: guests
					});
				}
			});
			return;		 	
		 })
		 .fail(function( err ){
			return res.format({
				html: function(){
		 			res.serverError()
				},
				json: function(){
					sails.log.error('EventController show|' + err + '|idEvent:' + req.param('id'));
					return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);				}
			});
		 }).done();
	},

	list: function(req, res, next) {
		res.view({
			authUser: req.user,
			events: req.events
		});
	},

	index: function(req, res, next) {
		var page = req.param('page') ? req.param('page') : 0,
		limit = req.param('limit') ? req.param('limit') : 20;
		Event.query('SELECT e.*, u.first_name, u.app_lang FROM events e LEFT JOIN users u ON e.id_owner = u.id LIMIT ' + page + ', ' + limit, function(err, events) {
			if (err) return next(err);
			sails.log.info('events: ' + JSON.stringify(events));
			res.view({
				authUser: req.user,
				events: events
			});
		});
	},

	share: function(req, res, next) {
		console.log('shareee');
		res.view({
			authUser: req.user,
			event: req.event,
			layout: 'baseLayout.ejs',
			baseLayout: 'event/baseShareLayout.ejs'			
		});
	},

	shareWith: function(req, res, next) {
		console.log(req.body);
		// res.view({
		// 	authUser: req.user,
		// 	event: req.event,
		// 	layout: 'baseLayout.ejs',
		// 	baseLayout: 'event/baseShareLayout.ejs'			
		// });
	},	

	edit: function(req, res, next) {

		var eventiDirPath  = ImageService.eventDirPath( req.param('id') );
		ImageService.readDir( eventiDirPath )
		.then(function( files ){

			var files = ImageService.getSpecificFiles( files, constants.mediumImgExtension )
				.map(function getEventImageDirPath( element, index, array ) {
					return {
						url: ImageService.relativeEventDirPath( req.param('id') ) + '/' + element,
						id: element.substring(0, 5) //id
					}
				});			
			// files.forEach(function logArrayElements(element, index, array) {
			//     console.log("file[" + index + "] = " + element);
			// });
			
			req.event.files = files;

			res.view({
				event: req.event.toJSON(),
				authUser: req.user,
				layout: 'baseLayout.ejs',
				baseLayout: 'event/baseEditLayout.ejs',
				is_public: constants.publicEvent,
				is_private: constants.privateEvent
			});		
		})
		.fail(function( err ){
			sails.log.error('EventController update|' + err + '|idEvent:' + req.param('id'));
			res.notFound();
		});		
	},

	update: function(req, res, next) {
		var eventiDirPath  = ImageService.eventDirPath( req.param('id') );
		//ParamnewEventImagePath and image validation (0_big.jpeg must exist)
		ImageService.readDir( eventiDirPath )
		.then(function( files ){
			if( files.length === 0 || !EventService.validateEventStep1(req.body) || !EventService.validateEventStep2(req.body) ) {
	    		return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
			}
			else {
				var deferred = Q.defer(), loc, date, queryStringArray, arrayQuery, query;

				//TODO ERASE
				sails.log.info('eventController: creating event');
				sails.log.info('ERASE user.id assignation');
				req.user = UserService.getUserFromRequest(req);



				//Update event with this information
				loc = LocalizationService.formatLocalization( req.param('localization') );
				date = DateService.formatDate( req.param('initDate') );

				arrayQuery = [
					req.param('title'),
					EventService.formatIdTitle( req.param('title') ),
					req.param('activity'),
					req.param('status'),
					req.param('publicEvent'),
					date,
					loc.city,
					loc.country,
					loc.name,
					loc.formatted_address,
					'POINT(' + loc.lat + ' ' + loc.lon + ')',
					req.param('free'),
					req.param('price'),
					req.param('total_participants'),
					req.param('duration'),
					req.param('equipment'),
					req.param('transport'),
					req.param('difficulty'),
					req.param('meetingPoint'),
					req.user.id,	
					req.param('id')
				]

				queryStringArray = ["UPDATE " + constants.dbName + ".events SET ",
											"title = ?, id_title = ?, id_sport = ?, id_status = ?, ",
											"is_public = ?, date_init = ?, ",
											"localization_city = ?, ",
											"localization_country = ?, ",
											"localization_name = ?, ",
											"localization_formatted_address = ?, ",
											"localization_point = GeomFromText(?), ",
											"free = ?, price = ?, ",
											"total_participants = ?, ",
											"duration = ?, equipment = ?, transport = ?, difficulty = ?, ",
											"meeting_point = ? ",
										"WHERE id_owner = ? AND id = ?"];

				query = Event.query( queryStringArray.join(''), arrayQuery, 
					function( err, result ) {
						var id_title;
				    	if( err ){
				    		res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
				    		deferred.reject( new Error( err ) );   		
				    		return;
				    	}

						if( req.param('status') == constants.eventPublished){
							res.json({ redirect: URLService.getCreateInvitationLink( req.param('id') )});
						} else {
							//TODO REDIRECT TO THE DASBOARD
							res.json({ redirect: URLService.getEventEditLink( req.param('id') )});
						}
						deferred.resolve();
						return;
					});

				return deferred.promise; 
			}
		})
		.fail(function( err ){
			sails.log.error('EventController update|' + err + '|idEvent:' + req.param('id'));
		});
	},

	destroy: function(req, res, next) {

		Event.destroy(req.event.id, function eventDestroyed(err) {
			if (err) return next(err);
			else res.redirect('/');
		});
	},

	/**
	* Overrides for the settings in `config/controllers.js`
	* (specific to EventController)
	*/
	_config: {}


};
