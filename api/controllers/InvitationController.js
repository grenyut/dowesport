var	validator = require('validator'),
	constants = require('../../constants.js');

/**
 * ShareController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
    // /invitation/edit/event/:id'
  	new: function(req, res, next) {
		res.view({
			authUser: req.user,
			event: req.event,
			layout: 'baseLayout.ejs',
			baseLayout: 'invitation/baseInvitationLayout.ejs'			
		});
	},

	/** 
	*	show the invitation with the event and the assistants
	**/
	show: function(req, res, next) {
		Invitation.findOne()
		.where({
			token: req.param('id'),
			email: req.param('e'),
			status: constants.invitationNoResponse
		}).then(function(invitation) {
			if(invitation) {
				//Find event
	           	var event = Event.findOne({
	           		id: invitation.id_event,
					id_status: constants.eventPublished	                   		
	           	}).then(function( event ){
	           		return event;
	           	});
				return [invitation, event]; 		
			} else {
				var errMsg = res.i18n( 'ERR_NO_INVITATION' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);				
			}
		}).spread(function(invitation, event){
			var params = {
				layout: 'baseLayout.ejs',
				baseLayout: 'invitation/baseShowLayout.ejs',
				invitation_token: invitation.token,
				email: invitation.email
			};

			if( event ) {
				params.event = event;
				res.view(params);                   			
       		} else {
				var errMsg = res.i18n( 'ERR_NO_EVENT_EXISTS_FOR_INVITATION' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);
       		}       		
		})
		.fail(function( err ){
			sails.log.error('invitationController show|' + err.message + '|id_token: ' + req.param('id') + '|email:' + req.param('email'));			
			res.redirect('/');
		});
	},		

	//POST /invitation
	create: function(req, res, next) {
		var emailList = req.param('emailList').split(',');

		req.user = UserService.getUserFromRequest(req);

		Event.findOne({
			id: req.param('id_event'), 
			id_owner: req.user.id
		}).then(function foundEvent(event) {

			if (!event) {
				var errMsg = res.i18n( 'ERR_NO_EVENT_EXISTS_FOR_INVITATION' );
				throw new Error(errMsg);
			} else {
				req.session.flash = { message: { msg: res.i18n( 'sendingInvitations' ) } };				
				res.json({ redirect: URLService.getRedirectRoomEventLink(event.id) });
			}

			req.event = event;
			
			// //Remove duplicates
			// emailList = emailList.filter(function(elem, pos) {
			//     return ( emailList.indexOf(elem) == pos );
			// });
			// //Remove self user
			// emailList = emailList.filter(function(elem, pos) {
			//     return ( elem != req.user.email );
			// });		
			//Combo Remove duplicates and Remove self user
			emailList.filter(function(elem, pos) {
			    return ( emailList.indexOf(elem) == pos && elem != req.user.email );
			});

			//Max 50 mails
			if( emailList.length > 50 ) {
				emailList = emailList.slice(0,49);
			}

			//send verification email
			emailList.forEach(function( email ) {
				if( !validator.isEmail(email) ) return;
				
				var newInvitation = {
					email: email,
					token: UtilsService.getRandomToken(50),
					id_event: req.event.id
				};
				
				Invitation.create( newInvitation )
					.exec(function( err, invitation ){
						if( !err ) {
							EmailService.dispatchInvitationEmail(res, req.user, req.event, invitation, function(err) {
								if (err) sails.log.error(err);
							});								
						} else sails.log.error(err);
					});
			});
		}).fail(function( err ) {
			sails.log.error(err.message + '|invitationController create id_event: ' + req.param('id_event'));
		    return res.json({ msg: err.message }, 404);
		});		
	},	

	//Accept or deny invitation
	//PUT /invitation
	update: function(req, res, next) {
		var status, token, validInvitationStatus;

		status = req.param('status');
		token = req.param('id');
		
		//Validate that the status value is 1 or 2. Cannot be 0
		validInvitationStatus = [ constants.invitationAccepted, constants.invitationDenied ];
		if( !validator.isIn(status, validInvitationStatus) ) {
			return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}

		Invitation.update({
			token: token,
			status: constants.invitationNoResponse,
			email: req.user.email
		}, {
			status: status
		})
		.then(function( invitations ){
			var invitation = invitations[0];

			//check that the invitation exists and that it's from the user logged
			if( invitation && invitation.email == UserService.getUserFromRequest(req).email ) {
				var event = Event.findOne({
	           		id: invitation.id_event,
					id_status: constants.eventPublished	                   		
	           	}).then(function( event ){
	           		return event;
	           	});

				return [invitation, event]; 
			} else {
				var errMsg = res.i18n( 'ERR_NO_INVITATION' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);			
			} 		
		})
		.spread(function( invitation, event ){
			if( event ){
				var eventOwner = User.findOne({
					id: event.id_owner 
				})
				.then(function( eventOwner ){
					return eventOwner;
				});			
				return [invitation, event, eventOwner];				
			} else {
				var errMsg = res.i18n( 'ERR_NO_EVENT_EXISTS_FOR_INVITATION' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);				
			}
		})
		.spread(function(invitation, event, eventOwner){
			if( eventOwner ){
				if( status == constants.invitationAccepted ) {
					//Redirect to room/event/id_event
					res.json({ redirect: URLService.getRedirectRoomEventLink(event.id) });					
				} else {
					req.session.flash = { message: { msg: res.i18n( 'invitationDeniedSuccess' ) } };				
					res.json({ redirect: '/' });					
				}

				//Send email
				User.findOne({ email: invitation.email }).then(function( user ) {
					if( status == constants.invitationAccepted ) {
						EmailService.dispatchInvitationAccepted(res, eventOwner, user, event, cb);
					} else {
						EmailService.dispatchInvitationDenied(res, eventOwner, user, event, cb);
					}					
				});
			} else {
				var errMsg = res.i18n( 'ERR_NO_OWNER_EXISTS_FOR_INVITATION' );
				req.session.flash = { err: { msg: errMsg } };
				throw new Error(errMsg);
			}
		})
		.fail(function( err ){
			sails.log.error('invitationController update|' + err.message + '|id_token: ' + req.param('id'));			
			res.redirect('/');
		});
	},	
	

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to ShareController)
   */
  _config: {}

  
};
