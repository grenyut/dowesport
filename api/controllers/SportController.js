/**
 * SportController
 *
 * @description :: Server-side logic for managing sports
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var	validator = require('validator');

module.exports = {
	search: function(req, res, next){
		Sport.find()
			.exec(function(err, sports) {
			  if(err) {
				sails.log.error('SportController search|' + err + '|searchTxt:' + req.param('searchText'));
			  	return res.json([]);
			  }
			  sports = SportService.filterSports( sports, req.userRequestLanguage );
			  return res.json(sports);
			});
	},

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to TestGraphController)
   */
  _config: {}		
};

