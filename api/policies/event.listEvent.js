module.exports = function(req, res, next) {

	var query = '';

	if (req.eventType || req.localization || req.query.date_init || req.query.date_end || req.query.duration_min || req.query.duration_max) {
		query = 'WHERE ';
		if (req.eventType) {
			query += 'e.id_sport =' + req.eventType + ' ';
		}
		if (req.localization) {
			query += query.length > 6 ? 'AND ' : '';
			query += 'e.localization LIKE \'%' + req.localization.split('\'').join('\\\'') + '%\' ';
		}
		if (req.query.date_init) {
			query += query.length > 6 ? 'AND ' : '';
			query += 'e.date_init >= \'' + req.query.date_init + '\' ';
		}
		if (req.query.date_end) {
			query += query.length > 6 ? 'AND ' : '';
			query += 'e.date_init <= \'' + req.query.date_end + '\' ';
		}
		if (req.query.duration_min) {
			query += query.length > 6 ? 'AND ' : '';
			query += 'e.duration >= \'' + req.query.duration_min + '\' ';
		}
		if (req.query.duration_max) {
			query += query.length > 6 ? 'AND ' : '';
			query += 'e.duration <= \'' + req.query.duration_max + '\' ';
		}
	}

	Event.query('SELECT e.*, u.first_name, u.app_lang FROM events e LEFT JOIN users u ON e.id_owner = u.id ' + query + 'LIMIT ' + req.page + ', ' + req.limit, function(err, events) {
		if (err) return next(err);
		else if (events.length == 0) return res.notFound();//TODO pàgina per introduïr nou event perque no hi ha
		else {
			req.events = events;
			next();
		}
	});

}
