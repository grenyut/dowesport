var constants = require('../../constants.js'),
	Q = require('q');


/**
* Checks if the place exists. If it doesn't, it's created
**/
module.exports = function(req, res, next) {

	var place = req.body.localization,
		userLang = req.userRequestLanguage,
		city = place.city,
		country = place.country,
		queryParams;
	
	if( city && country && city.length > 0 && country.length > 0 ) {


		// Places param validation
		if( !PlaceService.validatePlace( place ) ) {
			return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
		}	

		queryParams = [city, country, place.country_code];
		queryStringArray = ["SELECT * FROM " + constants.dbName + ".places WHERE ",
							"MATCH (city_" + userLang + ") AGAINST (?) AND ",
							"MATCH (country_" + userLang + ") AGAINST (?) AND ",
							"country_code = ?"];


		Place.query( queryStringArray.join(''), queryParams, function(err,result){
			if( err ){
				console.log(err);
			}else{
				console.log('Match query result:');
				console.log(result);
				if( result.length == 0 ) {
					var newPlace = {
						country_code: place.country_code
					};
					newPlace[ 'city_' + userLang ] = place.city;
					newPlace[ 'country_' + userLang ] = place.country;
					console.log('creating new place:', newPlace);
					Place.create( newPlace )
						.then(function( place ){
							console.log('Place created: ', place);
							req.place = place;
							next();
						})
						.fail(function(err){
							sails.log.error('place.checkExistence|' + err + '|place:' + JSON.stringify(newPlace));
							next();
						});
				} else {
					console.log('Place found: ', result[0]);
					req.place = result[0];
					next();
				}
			}
		});

	} else {
		next();
	}

};
