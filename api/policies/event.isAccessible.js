var constants = require('../../constants.js'),
	Q = require('q');


/**
* An accessible event means that it is public or the user
*  has an accepted invitation 
**/
module.exports = function(req, res, next) {

	Event.findOne()
		.where({ id: req.param('id') })
		.then(function( event ){
		 	if( !event ) throw new Error( 'event ' + req.param('id') + ' not found');
		 	req.event = event;
		 	//if private event look for an invitation
		 	if( constants.privateEvent == event.is_public && req.user) {
		 		return Invitation.findOne()
		 			.where({ 
		 				email: req.user.email,
		 				id_event: event.id
		 			});
		 	} else if( constants.privateEvent == event.is_public && !req.user) {
		 		throw new Error( 'NOT_INVITED' );
		 	} else return true;
		})
		.then(function( invitation ){
		 	if( invitation && invitation.status == constants.invitationNoResponse ) {
				throw new Error( 'NOT_ACCEPTED' ); 	
		 	} else if( req.event.is_public == constants.publicEvent || ( invitation && invitation.status == constants.invitationAccepted )) {
		 		next();
		 	} else {
		 		throw new Error( 'NOT_INVITED' );
		 	}
		})
		.fail(function( err ){
			sails.log.error('event.isAccessible|' + err + '|idEvent:' + req.param('id'));
		 	var message;
		 	if( err.message.indexOf('NOT_ACCEPTED') != -1 ) message = res.i18n( 'ERR_NO_INVITATION_ACCEPTED' );
		 	else message = res.i18n( 'ERR_NOT_FOUND_404' );
			return res.format({
				html: function(){

				 	if( err.message.indexOf('NOT_ACCEPTED') != -1 ){
				 		req.session.flash = { message: { msg: message } };	
				 		res.status(404);
				 		res.redirect('/');
		 			} else {
		 				res.notFound();
		 			}
				},
				json: function(){
					res.json({ msg: message }, 404);
				}
			});
		}).done();

};
