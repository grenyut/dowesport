var constants = require('../../constants.js');

module.exports = function createUserAvatar(req, res, next) {
    var dirPath = ImageService.profileDirPath( req.user.id ),
        userPath = dirPath + '/' + req.files.avatar.name,
        tmpDirPath = dirPath + 'tmp',
        fileName = new Date().getTime(),
        tmpUserPath = tmpDirPath + '/' + fileName + constants.imgExtension,        
        img_size = ImageService.imageSize(req.param('img_size'));


    ImageService.readFile( req.files.avatar.path )
        .then(function( fileData ) {
            req.fileData = fileData;
            return ImageService.makeDir( tmpDirPath ) 
        })
        .then(function(){
            if (req.files.avatar.size > 0) {
                //write file
                return ImageService.writeFile( tmpUserPath, req.fileData );
            } else {
                throw new Error('NOSIZE');
            }                   
        })
        .then( function() {
            return ImageService.readImageInfo( tmpUserPath, res );
        })
        .then(function( info ){
            req.info = info;
            return ImageService.checkImageWidth( tmpUserPath, tmpDirPath, info, constants.minProfileImgSize, constants.minProfileImgSize );           
        })
        .then(function(){
            var cropwidth, cropheight, x = 0, y = 0,
                destImagePath = tmpDirPath + '/' + fileName + constants.bigImgExtension + constants.imgExtension;
            
            cropwidth = ImageService.getImageMaxSize( req.info, constants.bigImgProfileSize );
            //Square profile image
            cropheight = cropwidth;

            
            console.log('---->resizeImages image 480 tmpUserPath:' + tmpUserPath);
            console.log('---->resizeImages image 480 destImagePath:' + destImagePath);  
            console.log('---->resizeImages image 480 info width:' + req.info.width);  
            console.log('---->resizeImages image 480 info height:' + req.info.height);  
            console.log('---->resizeImages image 480 cropwidth:' + cropwidth);               
            console.log('---->resizeImages image 480 cropheight:' + cropheight);               

            return ImageService.cropImage( tmpUserPath, destImagePath, cropwidth, cropheight, x, y );  
        })  
        .then(function( cropInfo ){
            var sizewidth, sizeheight,
                srcFile = tmpDirPath + '/' + fileName + constants.bigImgExtension + constants.imgExtension;
                srcImageFile = tmpDirPath + '/' + fileName + constants.smallImgExtension + constants.imgExtension;

            sizewidth = ImageService.getImageMaxSize( cropInfo, constants.smallImgProfileSize );

            console.log('---->resizeImages image 120 tmpUserPath:' + tmpUserPath);
            console.log('---->resizeImages image 120 srcImageFile:' + srcImageFile);  
            console.log('---->resizeImages image 120 size:' + sizewidth);              

            return ImageService.resizeImages(srcFile, srcImageFile, sizewidth);  
        })    
        .then(function(){
            var srcFile = tmpDirPath + '/' + fileName + constants.bigImgExtension + constants.imgExtension;
                srcImageFile = tmpDirPath + '/' + fileName + constants.thumbImgExtension + constants.imgExtension;
            
            console.log('---->resizeImages image 80 tmpUserPath:' + tmpUserPath);
            console.log('---->resizeImages image 80 srcImageFile:' + srcImageFile);            
            return ImageService.resizeImages(srcFile, srcImageFile, constants.thumbImgProfileSize);  
        })  
        //Remove old directory with the profile images
        .then(function() {
            return ImageService.removeDirectory( dirPath );
        }) 
        //Rename tmp to new directory
        .then(function() {
            return ImageService.rename( tmpDirPath, dirPath );
        })         
        .then(function(){
            req.user.avatar = ImageService.relativeProfileDirPath( req.user.id ) + '/' + fileName + img_size + constants.imgExtension;
            next();
        })                                       
        .fail(function( err ){
            sails.log.error('createUserAvatar: ' + err );
            ImageService.unlink( tmpUserPath )
                .then(function(){
                    console.log('----->removing ' + tmpUserPath)
                    return ImageService.removeDirectory( tmpUserPath )
                        .then(function( ){
                            return true;
                        })
                        .fail(function( err ){
                            deferred.reject(new Error( err.message + ':' + tmpUserPath ));
                        });
                })
                .fail(function( err ){
                    sails.log.error( err );
                });            
            if ( err.message == 'NOSIZE' ){
                next();
                return;
            } else if ( err.message === 'TOO_LITTLE' ){
                return res.json({ msg: res.i18n( 'ERR_IMAGE_TOO_SMALL' ) }, 500);
            } else if ( err.message === 'FORMAT_NO_SUPPORTED' ){
                return res.json({ msg: res.i18n( 'ERR_IMAGE_FORMAT_NOT_SUPPORTED' ) }, 500);
            }
            return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
        });
}
