var constants = require('../../constants.js');

module.exports = function(req, res, next) {
	//TODO Remove!
	req.user = UserService.getUserFromRequest(req);
	Invitation.findOne({
		id_event: req.param('id'), 
		email: req.user.email
	}).then(function( invitation ){
		console.log('EVENT.CHECKUSERINVITED1 invitation:');
		console.log(invitation);
		console.log('------------------');

		var errMsg;

		//user = users.filter(filter);
		//User not invited
		if( !invitation ) {
			errMsg = res.i18n( 'ERR_NO_USER_INVITATION' );
			req.session.flash = { err: { msg: errMsg } };
			throw new Error(errMsg);
		}
		//The invitation has been accepted
		if( invitation.status == constants.invitationAccepted ) {
			next();					
		} else { 
			//the user has not yet accepted the invitation
			errMsg = res.i18n( 'ERR_NO_INVITATION_ACCEPTED' );
			req.session.flash = { err: { msg: errMsg } };
			throw new Error(errMsg);
		}		
	}).fail(function( err ){
		sails.log.error('event.checkuserinvited err:' + err.message + '|id_event:' + req.param('id') + '|user_id:' + req.user.id);
		return res.redirect('/');		
	});
};
