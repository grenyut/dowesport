module.exports = function(req, res, next) {

	//TODO ERASE USER ID
	req.user = UserService.getUserFromRequest(req);
	Event.findOne({id: req.param('id'), id_owner: req.user.id}, function foundEvent(err, event) {

		sails.log.info('ERASE USER ID');

		if (err) {
			sails.log.error(err);
			return res.redirect('/');
		}
		else if (!event) return res.redirect('/');
		else {
			console.log("-> checkOwnEvent ok");
			req.event = event;
			next();
		}
	});

};
