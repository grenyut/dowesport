/**
 * Allow a logged-in user to see, edit and update her own profile
 * Allow admins to see everyone
 */

module.exports = function(req, res, ok) {
	var sessionUserMatchesId = false, 
	isAdmin = false;
	if( req.user ) {
		sessionUserMatchesId = req.user.id == req.param('id');
		isAdmin = req.user.admin;
	} 
	// The requested id does not match the user's id,
	// and this is not an admin
	if (!(sessionUserMatchesId || isAdmin)) {
		req.session.flash = {
			err: { msg: res.i18n( 'ERR_NO_ADMIN' ) }
		}
		sails.log.info('user cannot see profile! sessionUserMatchesId:' + sessionUserMatchesId +'| isAdmin: ' +isAdmin + '| req.user.id:'+req.user.id+ "| req.param('id'):"+req.param('id'));
	    res.redirect('/');
	    return;
	}
	ok();

};