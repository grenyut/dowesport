/**
 * Allow any authenticated user.
 */
module.exports = function (req, res, ok) {

  // User is allowed, proceed to controller
  if (req.user && req.user.admin) {
    return ok();
  }

  // User is not allowed
  else {

    // // User is not allowed
    // // (default res.forbidden() behavior can be overridden in `config/403.js`)
    // return res.forbidden('You are not permitted to perform this action.');    
		req.session.flash = {
			err: { msg: res.i18n( 'ERR_NO_ADMIN' ) }
		}
    return res.redirect('/');
  }
};