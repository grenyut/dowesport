var constants = require('../../constants.js');

module.exports = function (req, res, ok) {
  // User is allowed, proceed to controller
  if (req.isAuthenticated()) {
  	console.log('---->user authenticated');
    return ok();
  }

  // User is not allowed
  else {  	
  	console.log('---->user not authenticated');
    console.log('referer middleware: ' + req.header('Referer'));
    req.session.lastPage = req.header('Referer');    
    
    res.format({
      html: function(){
        res.redirect('/');
      },

      json: function(){
        res.json({ msg: res.i18n('ERR_NO_ADMIN') }, 401);
      }
    });
    return;
    //return res.redirect('/');
  }
};
