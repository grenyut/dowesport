var constants = require('../../constants.js'),
    Q = require('q');

var getCommand = function( srcPath, destPath, resizeWidth, resizeHeight, backgroundColor, cropWidth, cropHeight ){
    return "convert " + srcPath
      + " -resize " + resizeWidth + "x" + resizeHeight + " -background '" + backgroundColor
      + "' -gravity center "
      + " -extent " + cropWidth + "x" + cropHeight 
      + " -quality 92 " + destPath;
};

module.exports = function uploadImage(req, res, next) {
    var dirPath = ImageService.eventDirPath( req.param('id') ), 
        eventImagePath = dirPath + '/',
        imgNum = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000, //Img num between 10000 and 99999
        keyFile,
        query = constants.baseEventImgName;

    ImageService.readDir( dirPath ).then(function( files ){

        //The event cannot have more that 8 images (4 big, 4 medium)
        if( files.length > constants.maxNumUploadedImages ) {
            return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);            
        }

        req.file('eventImage').upload(function (err, uploadedFiles) {
            var uploadedFile;

            if( !req.param('id') ) {
                return res.json({ msg: res.i18n('ERR_BAD_REQUEST_400') }, 400);
            }   

            if ( uploadedFiles.length === 0 ) {
                return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
            }              

            uploadedFile = uploadedFiles[0];

            if ( uploadedFile.size <= 0 ) {
                return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
            }

            ImageService.readImageInfo( uploadedFile.fd )
                .then(function( info ) {
                    req.info = info;
                    return ImageService.makeDir( dirPath ) 
                })
                // large and medium image
             .then(function( info ){
                    var largeImageDestPath = eventImagePath + imgNum + constants.bigImgExtension + constants.jpegExtension,
                        mediumImageDestPath = eventImagePath + imgNum + constants.mediumImgExtension + constants.jpegExtension,
                        info = req.info,
                        cropWidth = constants.bigImgEventWidth,
                        cropHeight = constants.bigImgEventHeight, 
                        resizeHeight, resizeWidth, resizeAspectRatio,

                        mediumCropWidth = constants.mediumImgEventWidth,
                        mediumCropHeight = constants.mediumImgEventHeight, 
                        mediumResizeHeight, mediumResizeWidth, resizeAspectRatio; 

                        if( info.width > info.height ) { //horizontal image
                            resizeWidth = constants.bigImgEventWidth;
                            resizeHeight = Math.floor( info.height / info.width * resizeWidth );

                            mediumResizeWidth = constants.mediumImgEventWidth;
                            mediumResizeHeight = Math.floor( info.height / info.width * mediumResizeWidth );

                            return Q.all([
                                ImageService.resizeCropImages( uploadedFile.fd, largeImageDestPath, resizeWidth, resizeHeight, cropWidth, cropHeight, 0, 0 ),
                                ImageService.resizeCropImages( uploadedFile.fd, mediumImageDestPath, mediumResizeWidth, mediumResizeHeight, mediumCropWidth, mediumCropHeight, 0, 0 )
                            ]);
                        } else { //vertical image
                            resizeHeight = constants.bigImgEventHeight;
                            resizeWidth = Math.floor( info.width / info.height * resizeHeight );           
                            
                            mediumResizeHeight = constants.mediumImgEventHeight;
                            mediumResizeWidth = Math.floor( info.width / info.height * mediumResizeHeight );
                            
                            var mediumCommand = getCommand( uploadedFile.fd, 
                                                    mediumImageDestPath, 
                                                    mediumResizeWidth, 
                                                    mediumCropHeight, 
                                                    constants.backgroundImageColor, 
                                                    mediumCropWidth, 
                                                    mediumCropHeight );


                            var command = getCommand( uploadedFile.fd, 
                                                    largeImageDestPath, 
                                                    resizeWidth, 
                                                    resizeHeight, 
                                                    constants.backgroundImageColor, 
                                                    cropWidth, 
                                                    cropHeight );

                            return Q.all([
                                ImageService.gmExec(command),
                                ImageService.gmExec(mediumCommand)
                            ]);
                        }            
                })
                .then(function(){
                    //Return this value to show the uploaded image to the client
                    var relativeImgPath = [
                        ImageService.relativeEventDirPath( req.param('id') ),
                        '/',
                        imgNum,
                        constants.mediumImgExtension,
                        constants.jpegExtension,
                        '?',
                        new Date().getTime()
                    ];
                    req.mediumImageDestPath = {
                        url: relativeImgPath.join(''),
                        id: imgNum
                    }

                    next();
                })                                       
                .fail(function( err ){                            
                    if ( err.message == 'NOSIZE' ){
                        next();
                        return;
                    } else if ( err.message === 'TOO_LITTLE' ){
                        return res.json({ msg: res.i18n( 'ERR_IMAGE_TOO_SMALL' ) }, 500);
                    } else if ( err.message === 'FORMAT_NO_SUPPORTED' ){
                        sails.log.error('eventUpdateImage: ' + err.message );
                        return res.json({ msg: res.i18n( 'ERR_IMAGE_FORMAT_NOT_SUPPORTED' ) }, 500);
                    }
                    return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
                }).done();
        }); 
    })
    .fail(function( err ){
        sails.log.error('eventUpdateImage: ' + err );  
        return res.json({ msg: res.i18n( 'ERR_UNEXPECTED' ) }, 500);
    });
}
