module.exports = function(req, res, next) {
    req.page = req.param('page') ? req.param('page') : 0;
    req.limit = req.param('limit') ? req.param('limit') : 20;
    next();
}