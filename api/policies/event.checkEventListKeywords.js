module.exports = function(req, res, next) {

	if (req.param('keyword1') && !req.param('keyword2')) {
		EventType.findOneByDescription(
			req.param('keyword1')
		).exec(
			function(err, eventType) {
				if (err || !eventType) {
					//TODO buscar API Maps si existeis la localització i modificar per coordenades
					req.localization = req.param('keyword1');
					next();
				} else {
					req.eventType = eventType.id;
					next();
				}
			}
		);
	} else if (req.param('keyword1') && req.param('keyword2')) {
		EventType.findOneByDescription(
			req.param('keyword1')
		).exec(
			function(err, eventType) {
				if (err || !eventType) {
					res.notFound()
				} else {
					//TODO buscar API Maps si existeis la localització i modificar per coordenades
					req.eventType = eventType.id;
					req.localization = req.param('keyword2');
					next();
				}
			}
		);
	} else {
		next();
	}

};
