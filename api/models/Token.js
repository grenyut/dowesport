module.exports = {

	autoPK: false,
	schema: true,
	autoCreatedAt: false,
	autoUpdateddAt: true,
	tableName: 'tokens',

	attributes: {				

		remember_me_token: {
			required: true,
			type: 'string',
			maxLength: 64
		},

		id_user: {
			required: true,
			type: 'int'
		},		

		toJSON: function() {
			var obj = this.toObject();
			return obj;
		}		
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {
		next();
	}
}