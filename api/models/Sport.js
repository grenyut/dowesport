module.exports = {

	identity: 'Sport',

	schema: true,
	autoPK: true,
	//adapter: 'mysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'sports',
	//migrate: 'alter',

	attributes: {

		name_en: {
			type: 'string',
			maxLength: 45
		},		

		name_es: {
			type: 'string',
			maxLength: 45
		},	

		important: {
			type: 'int'
		},	

		//Colectivo: 0
		//Por equipos: 1
		//Iindividuales: 2
		type: {
			type: 'int'
		},								

		//inform that the sport is active or not
		//it has to be activated when an activity exists
		//0: sport disabled
		//1: sport enabled
		active: {
			type: 'int'
		},

		// Add a reference to User (NOT ACTUALLY WORKING)
		owners: {
			collection: 'User',
			via: 'sports'
		},		

		toJSON: function() {
			var obj = this.toObject();
			return obj;
		}
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {
		next();
	}

}