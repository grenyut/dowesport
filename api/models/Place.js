module.exports = {

	identity: 'Place',

	schema: true,
	autoPK: false,
	//adapter: 'mysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'places',
	//migrate: 'alter',

	attributes: {

		city_es: {
			type: 'string',
			maxLength: 72
		},		

		country_es: {
			type: 'string',
			maxLength: 72
		},	

		city_en: {
			type: 'string',
			maxLength: 72
		},	

		country_en: {
			type: 'string',
			maxLength: 72
		},

		country_code: {
			type: 'string',
			maxLength: 3,
			required: true
		},								

		toJSON: function() {
			var obj = this.toObject();
			return obj;
		}
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {
		next();
	}

}