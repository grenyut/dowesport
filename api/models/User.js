var crypto = require('crypto');

module.exports = {

	identity: 'User',
	
	schema: true,
	autoPK: true,
	//adapter: 'mysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'users',
	//migrate: 'alter',

	attributes: {
		
		first_name: {
			type: 'string',
			required: true,
			maxLength: 45
		},

		email: {
			type: 'string',
			email: true,
			required: true,
			unique: true,
			maxLength: 50
		},

		password: {
			type: 'string'
		},

		gender: {
			type: 'string',
			in: ['male', 'female', 'undisclosed']
		},

		dni: {
			type: 'string'
		},		

		avatar: {
			type: 'string'
		},

		country: {
			type: 'string',
			defaultsTo: 'n/a'
		},

		city: {
			type: 'string',
			defaultsTo: 'n/a'			
		},

		city_point: {
			type: 'json',
			point: true
		},		

		biography: {
			type: 'string'
		},

		online: {
			type: 'boolean',
			defaultsTo: false
		},

		birthdate: {
			type: 'date'
			// regex: "/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/", //YYYY-MM-DD
			// defaultsTo: '0000-00-00'
		},

		provider: {
			type: 'string'
		},	

		app_lang: {
			type: 'string'
		},		

		admin: {
			type: 'boolean',
			defaultsTo: false
		},
		// indicates that the account has been activated
		active: {
			type: 'boolean',
			defaultsTo: true
		},	

		active_token: {
			type: 'string'
		},	

		//length 100
		reset_password_token: {
			type: 'string'
		},	

		//fb, twitter...
		provider: {
			type: 'string'
		},	

		has_image: {
			type: 'boolean',
			defaultsTo: false			
		},

		sports: {
			collection: 'Sport',
			via: 'owners',
			dominant: true
		},	

		toJSON: function() {
			var obj = this.toObject();
			delete obj.password;
			delete obj.confirmation;
			delete obj.active_token;
			delete obj.reset_password_token;
			delete obj.active;
			delete obj._csrf;
			return obj;
		}		
	},

	beforeValidate: function (values, next) {
		// console.log('----->beforeValidation values: ');
		// console.log(values);
		// console.log('---END--');		
		if (typeof values.admin !== 'undefined') {
			if (values.admin === 'unchecked') {
				values.admin = false;
			} else  if (values.admin[1] === 'on') {
				values.admin = true;
			}
		}
		next();
	},	

	beforeCreate: function (values, next) {
		UserService.checkAndSetPassword(values, next);
		// console.log('----->beforeCreate values: ');
		// console.log(values);
		// console.log('---END--');
		// This checks to make sure the password and password confirmation match before creating record
	// 	if (!values.password || values.password != values.confirmation) {
	// 		return next({err: ["Password doesn't match password confirmation."]});
	// 	}

	// 	require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword) {
	// 		if (err) return next(err);
	// 		values.password = encryptedPassword;
	// 		values.active_token = crypto.createHash('md5').update("" + (new Date()).getTime()).digest("hex");
	// 		values.reset_password_token = crypto.createHash('md5').update("" + (new Date()).getTime()).digest("hex");
	// 		console.log('---->beforeCreate user active_token:' + values.active_token);
	// 		console.log('---->beforeCreate user reset_password_token:' + values.reset_password_token);
	// 		// values.online= true;
	// 		next();
	// 	});
	}
}