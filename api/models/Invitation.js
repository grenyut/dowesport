module.exports = {

	schema: true,
	autoPK: true,
	//adapter: 'mysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'invitations',

	attributes: {

		email: {
			type: 'email',
			required: true,
			maxLength: 50
		},

		token: {
			type: 'string',
			unique: true,
			maxLength: 50			
		},

		id_event: {
			type: 'integer',
			required: true
		},

		//0->no response
		//1->accepted
		//2->denied
		status: {
			type: 'integer',
			required: true,
			defaultsTo: 0
		},

        user: {
            model:'User'
        },						

		toJSON: function() {
			var obj = this.toObject();
			return obj;
		}
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {
		next();
	}

}