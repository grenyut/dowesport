module.exports = {

	schema: true,
	autoPK: true,
	// adapter: 'remoteMysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'events',

	types: {
		point: function(latlng){
			return latlng.x && latlng.y;
		}
	},	

	attributes: {

		id_owner: {
			type: 'integer',
			required: true
		},

		id_title: {
			type: 'string',
			maxLength: 136
		},

		title: {
			type: 'string',
			required: true,
			maxLength: 100
		},

		id_sport: {
			type: 'integer',
			required: true
		},

		description: {
			type: 'string',
			maxLength: 500	
		},

		total_participants: {
			type: 'integer',
		},

		date_init: {
			type: 'datetime',
			required: true
		},

		duration: {
			type: 'string',
			maxLength: 50
		},

		equipment: {
			type: 'string',
			maxLength: 255
		},

		transport: {
			type: 'string',
			maxLength: 255
		},

		//1 free
		//0 paying
		free: {
			type: 'integer',
			defaultsTo: 1
		},

		price: {
			type: 'float',
			defaultsTo: 0
		},				
		
		// '0','published'
		// '1','draft'
		// '2','expired'
		// '3','deleted'
		// '4','deleted by admin'
		id_status: {
			type: 'integer',
			required: true
		},

		id_place: {
			type: 'integer',
		},		

		//LOCALIZATION info
		localization_city: {
			type: 'string',
			maxLength: 72
		},

		localization_country: {
			type: 'string',
			maxLength: 72
		},

		localization_formatted_address: {
			type: 'string',
			maxLength: 255
		},
		//name returned by the google autocomplete API
		localization_name: {
			type: 'string',
			maxLength: 255
		},		

		localization_point: {
			type: 'json',
			point: true
		},

		// //MEETING POINT info
		// meeting_city: {
		// 	type: 'string',
		// 	maxLength: 255
		// },

		// meeting_country: {
		// 	type: 'string',
		// 	maxLength: 255
		// },

		// meeting_formatted_address: {
		// 	type: 'string'//'point'
		// },

		meeting_point: {
			type: 'string',
			maxLength: 255
		},

		is_public: {
			type: 'boolean',
			required: true,
			defaultsTo: 1
		},

		//0 -> easy
		//1 -> moderate
		//2 -> difficult
		//3 -> extremo
		difficulty: {
			type: 'integer',
			required: true,
			defaultsTo: '0'
		},


		toJSON: function() {
			var obj = this.toObject();
			var d = new Date(obj.date_init);
			console.log('evenTOJSON');
			console.log(DateService.formatDate2( d ));			
			obj.date_init = DateService.formatDate2( d );
			return obj;
		}
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {		
		next();
	}

}
