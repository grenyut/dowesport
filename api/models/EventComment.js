module.exports = {

	schema: true,
	autoPK: true,
	//adapter: 'mysql',
	autoCreatedAt: true,
	autoUpdateddAt: true,
	tableName: 'event_comments',

	attributes: {

        user: {
            model:'User',
            columnName: 'id_user'
        },

		id_event: {
			type: 'int',
			required: true
		},

		message: {
			type: 'string',
			required: true
		},

		toJSON: function() {
			var obj = this.toObject();
			var user = obj.user;
			delete user.password;
			delete user.confirmation;
			delete user.active_token;
			delete user.reset_password_token;
			delete user.email;
			delete user.gender;
			delete user.dni;
			delete user.biography;
			delete user.online;
			delete user.birthdate;
			delete user.provider;
			delete user.app_lang;
			delete user.admin;
			delete user.updatedAt;
			delete user.country;
			delete user.city;
			delete user.city_point;
			obj.user = user;
			delete obj.updatedAt;
			delete obj.id_event;
			return obj;
		}
	},

	beforeValidate: function(values, next) {
		next();
	},

	beforeCreate: function(values, next) {
		next();
	}

}