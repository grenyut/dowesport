var constants = require('../../constants.js'),
	validator = require('validator');	

exports.validatePlace = function( place ) {

	console.log('city ' + place.city );
	console.log('country ' + place.country );
	console.log('country code' + place.country_code );
	// console.log('lat float ' + validator.isFloat( place.location.lat ));
	// console.log('log float ' + validator.isFloat( place.location.lon ));
	// console.log('alpha city ' + ( place.city ? validator.isAlpha( place.city ) : true ));
	// console.log('alpha country ' + StringService.isAlpha( place.country ));

	return UtilsService.validCoords( place.location.lat, place.location.lon )
		&& StringService.isAlpha( place.country_code )
		&& place.country_code.length >= 2
		&& place.country_code.length <= 3
		&& validator.isFloat( place.location.lat )
		&& validator.isFloat( place.location.lon )
		&& ( place.city ? StringService.isAlpha( place.city ) : true )
		&& StringService.isAlpha( place.country )
		&& StringService.isAlpha( place.formatted_address );
}