var passport = require('passport')
    , FacebookStrategy = require('passport-facebook').Strategy
    , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
    , LocalStrategy = require('passport-local').Strategy
    , RememberMeStrategy = require('passport-remember-me').Strategy
    , utils = require('../../utils.js')
    , constants = require('../../constants.js');


//helper functions
var findByEmail = function findByEmail( email, next ) {
    console.log('middleware findByEmail: ' + email);
    User.findOne({
        email: email,
        active: 1
    }, function foundUser( err, user ) {
        if ( err ) {
            sails.log.error(err); 
            return next( err );
        }
        if ( !user ){
            console.log('no user found with email: ' + email);
            return next( null, false );
        }
        next( null, user );
    }); 
};

var verifyHandler = function (token, tokenSecret, profile, next) {
    var userObj, photoURL = '';
    if( profile.photos && profile.photos[0] )
        photoURL = profile.photos[0].value;
    console.log('middleware verifyHandler: ' + profile.emails[0].value);
    process.nextTick( function () {
        User.findOneByEmail( profile.emails[0].value, function foundUser( err, user ) {
            if (user) {
                return next(null, user);
            } else {
                userObj = {
                    first_name: profile.displayName,
                    email: profile.emails[0].value,
                    password: 'toEnter',
                    confirmation: 'toEnter',
                    provider: profile.provider,
                    gender: profile.gender,
                    avatar: photoURL,
                    online: true
                }
                User.create(userObj, function userCreated(err, user) {
                    return next(err, user);
                });
            }
        });
    });
};

var verifyCustomLoginHandler = function ( email, password, next ) {
    // asynchronous verification, for effect...
    process.nextTick( function() {
        console.log('middleware verifyCustomLoginHandler: ' + email);

        // Find the user by email. If there is no user with the given
        // email, or the password is not correct, set the user to `false` to
        // indicate failure and set a flash message. Otherwise, return the
        // authenticated `user`.
        findByEmail( email, function ( err, user ) {
            if ( err ) {
                sails.log.error(err);
                return next( err );
            }
            if ( !user ) {
                return next(null, false);
            }
            require('bcrypt').compare(password, user.password, function (err, res) {
                if (err) {
                    return next(err);
                }
                next (null, res ? user : false);
            });
        })
    });
};



// Passport session setup.
// To support persistent login sessions, Passport needs to be able to
// serialize users into and deserialize users out of the session. Typically,
// this will be as simple as storing the user ID when serializing, and finding
// the user by ID when deserializing.
//Method called when user logs req.logIn!
passport.serializeUser( function( user, next ) {
    console.log('middleware serializeUser: ' + user.email);
    User.update({
        email: user.email
    }, {
        online: true
    }).then(function( users ){
        next( null, users[0].email );
    }).fail(function( err ){
        next( err, false );
    });
});
 
 //Called for each request that needs authentication (req.isAuthenticated)
passport.deserializeUser( function( email, next ) {
    User.findOne({
        email: email
    }, function foundUser( err, user ) {
        console.log('middleware deserializeUser: ' + user.email);
        next( err, user );
    });
});


function consumeRememberMeToken(token, fn) {
    // invalidate the single-use token
    console.log('middleware consumeRememberMeToken: ' + token);
    Token.findOne({ remember_me_token: token }, function( err, tokenRow ) {
        if( err ) return fn( err );
        if(!tokenRow) return fn( null, false );

        Token.query('delete from ' + constants.dbName + '.tokens where remember_me_token = ?', [token],
            function(err){
                if( err ) {
                    return fn(err);
                } 
                return fn(null, tokenRow.id_user);
            });

        // Token.destroy({ 
        //     remember_me_token: token
        // }).exec(function userUpdated( err ) {
        //     console.log('----->consumeRememberMeToken');
        //     console.log(tokenRow);
        //     console.log(err);
        //     console.log('----->FINconsumeRememberMeToken');

        //     if( err ) {
        //         return fn(err);
        //     } 
        //     return fn(null, tokenRow.id_user);
        // }); 
    });
};


function issueToken(user, done) {
    var token = utils.randomString(64);
    Token.create( { remember_me_token: token, id_user: user.id } , function userUpdated( err ) {
        // console.log('----->issueToken');
        // console.log({ remember_me_token: token, id_user: user.id });
        // console.log(err);
        if (err) { return done(err); }
        return done(null, token);
    }); 
};

module.exports = {
	configProviders: function(options, next) {

        passport.use( new FacebookStrategy({
                clientID: "447172325345376",
                clientSecret: "8f4d6ce8c56a37818e08584cdd427d25",
                callbackURL: "http://localhost:1337/auth/facebook/callback"
            },
            verifyHandler
        ));

        passport.use( new GoogleStrategy({
                clientID: 'YOUR_CLIENT_ID',
                clientSecret: 'YOUR_CLIENT_SECRET',
                callbackURL: 'http://localhost:1337/auth/google/callback'
            },
            verifyHandler
        ));

        passport.use( new LocalStrategy({
                usernameField: 'email',
                passwordField: 'password'
            },
            verifyCustomLoginHandler
        ));       

        passport.use(new RememberMeStrategy(
            function(token, done) {
                consumeRememberMeToken(token, function(err, uid) {
                    if (err) { 
                        sails.log.error(err);
                        return done(err); 
                    }
                    if (!uid) { 
                        return done(null, false); 
                    }
                    User.findOne(uid, function foundUser(err, user) {
                        // console.log('----->consumeRememberMeToken findOne');
                        // console.log(err);
                        // console.log(user);                            
                        // console.log('.........................-----');                            
                        if (err) { return done(err); }
                        if (!user) { return done(null, false); }
                        return done(null, user);
                    });
                    
                });
              },
              issueToken
        ));               
	}
};