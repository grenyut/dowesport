var constants = require('../../constants.js');


exports.validCoords = function validCoords(number_lat,number_lng) {
    if (inrange(-90,number_lat,90) && inrange(-180,number_lng,180)) {
        return true;
    }
    else return false;
}

function inrange(min,number,max){
    if ( !isNaN(number) && (number >= min) && (number <= max) ){
        return true;
    } else return false;
}

/**
 * Generate a random token(letters are in uppercase).
 *
 * @param {int} length -> the desired number of characters
 *
 * @return {string} random alphanumeric code
 */
exports.getRandomToken = function getRandomToken(length){
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
	var token = '';

	for (var i = 0; i < length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		token += chars.substring(rnum,rnum+1);
	}

	return token;
};

/**
 * @param {array} Array of accepted languages (req.acceptedLanguages)
 * @return {string} one of the supported languages ('es', 'en')
**/
exports.getSupportedLang = function getSupportedLang( acceptedLangs ){
	var defaultLang = [],
		supportedLangs = sails.config.i18n.locales,
		found;

	acceptedLangs.filter(function( lang ) {
		found = false;
		supportedLangs.forEach(function( elem, index, array ){
			if( lang.indexOf( elem ) != -1 ) {
				defaultLang.push( elem );
				found = true;
				return false;
			}
		});
		if( found ) return true;
		else return false;
	});
	return (defaultLang.length > 0) ? defaultLang[0] : sails.config.i18n.defaultLocale;
};