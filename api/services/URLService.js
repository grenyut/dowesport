var constants = require('../../constants.js');

exports.getRegisterConfirmationButtonLink = function getRegisterConfirmationButtonLink( user ) {
	return constants.rootUrl + 'verify' + '?e=' + user.email + '&h=' + user.active_token;
};

exports.getRemoveRegistrationBruttonLink = function getRemoveRegistrationBruttonLink( user ) {
	return constants.rootUrl + 'removeaccount' + '?e=' + user.email + '&h=' + user.active_token;
};

/**
*	acceptInvitation values can be "accept" or "deny"
**/
exports.getInvitationButtonLink = function getInvitationButtonLink( invitation, acceptInvitation ) {
	return constants.rootUrl + 'invitation/' + acceptInvitation + '?e=' + invitation.email + '&h=' + invitation.token;
};
/**
*	url to  values can be "accept" or "deny"
**/
exports.getCreateInvitationLink = function getCreateInvitationLink( id_invitation ) {
	return '/invitation/edit/' + id_invitation;
};
/**
*	Description: creates an invitation link that let the access to an event
**/
exports.getInvitationEventLink = function getInvitationEventLink( invitation ){
	return constants.rootUrl + constants.invitationDetail + invitation.token + '?e=' + invitation.email;
};

exports.getEventLink = function getEventLink( event ) {
	return constants.rootUrl + 'event/' + event.id + '/' + event.id_title;
};

exports.getEventEditLink = function getEventEditLink( idEvent ) {
	return '/event/edit/' + idEvent;
};

exports.getResetPasswordLink = function getResetPasswordLink( user ) {
	return constants.rootUrl + 'reset_password' + '?secret=' + user.reset_password_token;
};

/**
*	Description: creates the link to an event room
**/
exports.getRedirectRoomEventLink = function getRedirectRoomEventLink( id_event ){
	return constants.roomEventDetail + id_event;
};