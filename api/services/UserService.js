var constants = require('../../constants.js'),
	validator = require('validator'),
    crypto = require('crypto'),
    utils = require('../../utils.js'),
    neo4j = require('neo4j'),
	db = new neo4j.GraphDatabase('http://localhost:7474');

exports.getUserFromRequest = function(req) {
	//TODO CHANGE THISSSSSS!
	sails.log.info('CHANGE THISSSSSS! getUserFromRequest');
    switch(process.env.NODE_ENV){
        case constants.devEnv:
            return {
            	id: 7, 
            	email: 'grenyut@gmail.com',
            	name: 'marc'
            };

        case constants.prodEnv:
            return req.user;

        default:
            return req.user;
    }
};

exports.validateUserCoords = function validateUserCreation( data ) {
		return validator.isFloat( data.lat )
		&& validator.isFloat( data.lon )
		&& UtilsService.validCoords( data.lat, data.lon );
};

exports.getAvatarUrl = function(user, img_size) {
    if (!user.avatar) {
        return constants.staticUsersImgURI + constants.avatarName + img_size + constants.imgExtension;
    } else {
        return constants.staticUsersImgURI + user.id + '/' + constants.avatarName + img_size + constants.imgExtension;
    }
};

exports.checkAndSetPassword = function(values, next) {
	if (!values.password || values.password != values.confirmation) {
		return next({err: ["Password doesn't match password confirmation."]});
	}

	require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword) {
		if (err) {
			console.log(err)
			return next(err);
		}
		values.password = encryptedPassword;
		values.active_token = crypto.createHash('md5').update("" + (new Date()).getTime()).digest("hex");
		values.reset_password_token = utils.randomString(100);
		console.log('---->beforeCreate user active_token:' + values.active_token);
		console.log('---->beforeCreate user reset_password_token:' + values.reset_password_token);
		// values.online= true;
		return next();
	});
};

exports.setUserOnlineStatus = function( userId, status ) {
	User.update(userId, {online: status}, function(err, users) {
		if (err) {
			sails.log.error('userService setUserOnlineStatus|' + err.message + '|userId: ' + userId + '|status:' + status);			
		}
	});
};


/////////////////
//////Neo4J//////
/////////////////
exports.getKnownEmails = function(user, callback) {

	var params = {
		emailFrom: user.email
	};

// MATCH (a:Person { email:'marc' })-[:KNOWS]->(m:Person)
// RETURN m.email

	var query = [
		"MATCH (a:Person {email: {emailFrom}}) -[:KNOWS]- (m:Person)",
		"RETURN m.email",
		"LIMIT 100"
	].join('\n');		

	db.query(query, params, function (err, results) {
		console.log(results);
		if (err) {
			sails.log.error(err);
			callback(err);
		}
		else return callback(null, results);
	});
};

exports.setKnowRelationship = function(user, emailTo, callback) {
	var params = {
		emailFrom: user.email,
		emailTo: emailTo
	};

	// MATCH (lft:Person { email:'marc' }),(rgt:Person)
	// WHERE rgt.email = 'tossio'
	// CREATE UNIQUE (lft)-[r:KNOWS]->(rgt)
	// RETURN r

	//If we want to create more than one relationship
	//we have to set "IN ['A','B']" in the WHERE closure
	var query = [
		"MATCH (lft:Person {email: {emailFrom}}),(rgt:Person)",
		"WHERE rgt.email = {emailTo}",
		"CREATE UNIQUE (lft)-[r:KNOWS]->(rgt)",
		"RETURN r"
	].join('\n');

	db.query(query, params, function (err, results) {
		console.log(results);
		if (err) {
			sails.log.error(err);
		}
	});
};

exports.createPersonNode = function(user, callback) {
	var query = [
		'MERGE (p:Person {email}) ',
		'RETURN p'
		].join('\n'),
		params = {
			props: {
				email: user.email
			}
		};

	db.query(query, params, function (err, results) {
		console.log(results);
		console.log(err);
		if (err) {
			sails.log.error(err);
		}
		else sails.log.info(results);
		return;
	});
};