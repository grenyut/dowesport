var constants = require('../../constants.js'),
	fs = require('fs'),
	easyimg = require('easyimage'),
	rimraf = require('rimraf'),
	mkdirp = require('mkdirp'),
	Q = require('q');

exports.relativeEventDirPath = function( eventId ){
	return constants.relativeEventsImgURI + eventId;
} 

exports.eventDirPath = function( eventId ){
	return process.cwd() + constants.saveEventsImgURI + eventId;
} 

exports.relativeProfileDirPath = function( eventId ){
	return constants.relativeUsersImgURI + eventId;
} 

exports.profileDirPath = function( userId ){
	return process.cwd() + constants.saveUsersImgURI + userId;
} 

exports.imageSize = function(img_size) {
	return (img_size && (img_size == constants.thumbImgExtension || img_size == constants.smallImgExtension || img_size == constants.bigImgExtension)) ? img_size : constants.bigImgExtension;
}

exports.getImageMaxSize = function getImageMaxSize( info, maxSize ){
    var maxWidth;
    if( info.width > info.height ) { //horizontal image
        maxWidth = info.height;
    } else { //vertical image
        maxWidth = info.width;
    }

    //Square profile image
    if( maxWidth > maxSize ) return maxSize;
    else return maxWidth;
};

exports.readDir = function readDir( path ) {
	return Q.nfcall( fs.readdir, path );
};

exports.readFile = function readFile( path ) {
	return Q.nfcall( fs.readFile, path );
};

exports.writeFile = function writeFile( path, data ) {
	return Q.nfcall( fs.writeFile, path, data );
};

exports.rename = function rename( oldPath, newPath ) {
	return Q.nfcall( fs.rename, oldPath, newPath );
};

exports.exists = function exists( path ) {
	return Q.nfcall( fs.exists, path );
};

exports.unlink = function unlink( path ) {
	return Q.nfcall( fs.unlink, path );
};

exports.removeDirectory = function removeDirectory( dirPath ) {
	var deferred = Q.defer();
    rimraf(dirPath, function( err ) {
        if (err){
        	sails.log.error( err );
        	//deferred.reject( new Error(err) );
        }
        deferred.resolve();
    });

	return deferred.promise;    
};

exports.makeDir = function makeDir(dirPath) {
	var deferred = Q.defer();
    mkdirp(dirPath, '777', function(err) {
        if (err && err.errno != 47) {
            sails.log.error(err);
            deferred.reject(new Error(err));
        } else {
        	deferred.resolve();
        }
    });

	return deferred.promise;       
}

exports.readImageInfo = function readImageInfo( path ) {

	return easyimg.info( path ).then(
		function(info) {
			return info;
		}, function ( err ) {
            if( err.message.indexOf('supported') != -1 ) {
            	throw new Error('FORMAT_NO_SUPPORTED');
            } else throw new Error(err);
		}
	);
};

exports.checkImageWidth = function checkImageWidth( filePath, dirPath, info, width, height ) {
	var deferred = Q.defer();

	if ( info.width < width || info.height < height ) {
		deferred.reject( new Error('TOO_LITTLE') );
	} else {
		deferred.resolve( info );
	}

    return deferred.promise;  
};

exports.convertImage = function convertImage(path) {
	return easyimg.convert({
        src: path,
        dst: path.split('.')[0] + '.png'
    })
	.then( function( image ) {
		return true;
	})
	.fail(function ( err ) {			
		throw new Error(err);
	});  
};

exports.resizeImages = function resizeImages(srcPath, destPath, width, height) {
	var params = {
	        src: srcPath,
        	dst: destPath,
        	width: width		
		};

	if( height ) params.height;

	return easyimg.resize(params)
		.then( function( image ) {
			return true;
		})
		.fail(function ( err ) {			
			throw new Error(err);
		});  
};

exports.cropImage = function cropImage( srcPath, destPath, cropwidth, cropheight, x, y ) {

	var deferred = Q.defer(),
		params = {
	        src: srcPath,
	        dst: destPath,
	        cropwidth: cropwidth,
	        cropheight: cropheight,
	        x: x, y: y
	    };

	return easyimg.crop(params)
		.then( function(image) {
			return {
            	width: cropwidth,
            	height: cropheight
            };
		})
		.fail(function ( err ) {			
			throw new Error(err);
		}); 
};

exports.resizeCropImages = function resizeCropImages(srcPath, destPath, width, height, cropwidth, cropheight, x, y) {
	// var deferred = Q.defer(),
	var params = {
	        src: srcPath, dst: destPath,
	        width: width,
	        height: height,
	        cropwidth: cropwidth, 
	        cropheight: cropheight,
	        x: x, y: y
	    };

	return easyimg.rescrop(params)
		.then( function(image) {
			return image;
		})
		.fail(function ( err ) {			
			throw new Error(err);
		}); 
};

exports.gmExec = function resizeCropImages( command ) {
	return easyimg.exec( command )
		.then( function( file ) {
			return true;
		}, function ( err ) {	
			throw new Error(err);
		});
};

exports.getSpecificFiles = function getSpecificFiles( files, extension ) {
	return files.filter(function( element ){
		return element.indexOf( extension ) != -1;
	});
};


