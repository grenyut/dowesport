module.exports.filterSports = function( sportList, lang ) {
    return sportList.map(function(elem){
        return {
            id: elem.id,
            name: elem['name_' + lang],
            userPref: elem.userPref
        }
    });
};

module.exports.filterSport = function( sport, lang ) {
    return {
        id: sport.id,
        name: sport['name_' + lang],
        userPref: sport.userPref
    }
};