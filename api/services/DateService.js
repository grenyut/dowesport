var constants = require('../../constants.js');

exports.validDate = function validDate(date) {
	//dd/MM/YYYY HH:mm
	var pattern = new RegExp(/^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})$/);
	return pattern.test(date);
}

//FROM 'DD/MM/YYYY HH:MM:SS' to 'YYYY-MM-DD HH:MM:SS'
exports.formatDate = function( date ) {
	var test = this.validDate( date ),
		dateTime, date, time;
	if( test ) {
		dateTime = date.split(' ');
		date = dateTime[0].split('/');
		time = dateTime[1].split(':');
		//'YYYY-MM-DD HH:MM:SS'
		console.log(date[2] +'/'+ date[1] +'/'+ date[0] +' '+ time[0] +':'+ time[1]);
		return date[2] +'/'+ date[1] +'/'+ date[0] +' '+ time[0] +':'+ time[1];
	} else return false;
}

//FROM DATE object to 'YYYY/MM/DD HH:MM:SS'
exports.formatDate2 = function( date ) {
	return getValueTwoDigits( date.getDate() ) + '/' + getValueTwoDigits( (date.getMonth() + 1) ) + '/' + date.getFullYear() + ' ' +  
			getValueTwoDigits( date.getHours() ) + ':' + getValueTwoDigits( date.getMinutes() );
}

function getValueTwoDigits( val ) {
    if ( val < 10 ){
        return ('0' + val);
    } else {
        return val.toString();
    }
}


exports.getDate = function getDate( date ) {
	var test = this.validDate( date ),
		dateTime, date, time;
	if( test ) {
		dateTime = date.split(' ');
		date = dateTime[0].split('/');
		time = dateTime[1].split(':');
		return new Date(date[2], date[1], date[0], time[0], time[1], 00, 00);
	} else return false;
}

exports.isDateGreaterThanToday = function( date ) {
	var currentDate = new Date();
	if (date > currentDate) return true;
	else return false;
}