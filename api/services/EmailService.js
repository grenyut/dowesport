var constants = require('../../constants.js'),
	nodemailer = require("nodemailer"),
	path           = require('path'),
  	templatesDir   = path.resolve(__dirname, '../..', 'emailTemplates'),
  	emailTemplates = require('email-templates-windows'),
	EM = {},
	EM_utils = {},
	EmailAddressRequiredError = new Error('email address required');


module.exports = EM;

EM.utils = EM_utils;

EM_utils.getDisplayUsername = function(user){
	return user.name;// + ' ' + (user.surname1 ? user.surname1 : user.lastname) ;
};

// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "sharesportevents@gmail.com",
        pass: "sharesport"
    }
});

EM_utils.i18n_htmlReplaceValue = function (user){
	// return a new object so we can add stuff
	var ret = {};
	ret.user = {};
	ret.link = {};
	ret.text = {};

	if (typeof user != 'undefined') {
		ret.user.userName = EM.utils.getDisplayUsername(user);
	}
	return ret;
};

//////////////////////////////////////////////////////////////
////////////////////  mail any user //////////////////////
//////////////////////////////////////////////////////////////

/**
 * Register confirmation email dispatcher
 */

// EM.dispatchRegisterConfirmationEmail = function(res, user, callback) {
// 	// note: receive user data from formulario NOT from DB !
// 	var replaceValue = EM.utils.i18n_htmlReplaceValue( user );
// 		replaceValue.email = user.email;
// 		replaceValue.subject = res.__('emailList.registerConfirmation.subject');
// 		replaceValue.link.buttonLink = URLService.getRegisterConfirmationButtonLink( user );
// 		replaceValue.text.subject = replaceValue.subject;
// 		replaceValue.text.oneMoreStep = res.__('emailList.registerConfirmation.oneMoreStep');
// 		replaceValue.text.clickButton = res.__('emailList.registerConfirmation.clickButton');
// 		replaceValue.text.activateAccount = res.__('emailList.registerConfirmation.activateAccount');


// 	sendOne('registerConfirmationTempl', replaceValue, function (err, responseStatus, html, text) {
// 		if(err)
// 			sails.log.error('email error: ' + err + '|registerConfirmation|responseStatus:' + responseStatus);       
//      })
// };

//TODO test
EM.dispatchNewRegistrationEmail = function(res, user, callback) {
	// note: receive user data from formulario NOT from DB !
	var replaceValue = EM.utils.i18n_htmlReplaceValue( user );
		replaceValue.email = user.email;
		replaceValue.subject = res.__('emailList.newRegistration.subject');
		replaceValue.link.buttonLink = URLService.getRemoveRegistrationBruttonLink( user );
		replaceValue.text.subject = replaceValue.subject;
		replaceValue.text.intro = res.__('emailList.newRegistration.intro');
		replaceValue.text.presentation = res.__('emailList.newRegistration.presentation');
		replaceValue.text.deleteRegistration = res.__('emailList.newRegistration.deleteRegistration');
		replaceValue.text.deleteRegistrationLink = res.__('emailList.newRegistration.deleteRegistrationLink');


	sendOne('newRegistration', replaceValue, function (err, responseStatus, html, text) {
		if(err)
			sails.log.error('email error: ' + err + '|registerConfirmation|responseStatus:' + responseStatus);       
     })
};


/**
*	Invitation send to a list of emails
*	User is the event owner
*	Invitation has the email of the addressee
**/
EM.dispatchInvitationEmail = function(res, user, event, invitation, callback) {
	// note: receive user data from formulario NOT from DB !
	var replaceValue = EM.utils.i18n_htmlReplaceValue( user );
	replaceValue.email = invitation.email;
	replaceValue.subject = res.__('emailList.invitation.subject');
	replaceValue.link.eventInvitation = URLService.getInvitationEventLink( invitation );
	//replaceValue.link.event = URLService.getEventLink( event );
	replaceValue.text.subject = replaceValue.subject;
	replaceValue.text.intro1 = res.__('emailList.invitation.intro1');
	replaceValue.text.intro2 = res.__('emailList.invitation.intro2');
	replaceValue.text.date = res.__('emailList.invitation.date');
	replaceValue.text.location = res.__('emailList.invitation.location');
	replaceValue.text.noLocation = res.__('emailList.invitation.noLocation');
	replaceValue.text.clickButton = res.__('emailList.invitation.clickButton');
	replaceValue.text.eventTitle = event.title;
	replaceValue.text.eventDate = event.date_init;
	replaceValue.text.eventFormattedAddress = event.localization_formatted_address;
	replaceValue.text.eventAccept = res.__('emailList.invitation.eventAccept');

	sendOne('invitationToEventTempl', replaceValue, function (err, responseStatus, html, text) {
		if(err)
			sails.log.error('email error:' + err + '|invitation|responseStatus:' + responseStatus);
    })
};

EM.dispatchInvitationDenied = function(res, user, userFail, event, callback) {
	// note: receive user data from formulario NOT from DB !
	var replaceValue = EM.utils.i18n_htmlReplaceValue( user );
	replaceValue.email = invitation.email;
	replaceValue.subject = res.__('emailList.invitation.deny.subject') + ': ' + event.title;
	//replaceValue.link.event = URLService.getEventLink( event );
	replaceValue.text.subject = replaceValue.subject;
	replaceValue.text.intro1 = res.__('emailList.invitation.deny.intro1');
	replaceValue.text.intro2 = res.__('emailList.invitation.intro2');
	replaceValue.text.intro3 = res.__('emailList.invitation.intro3');
	replaceValue.text.intro4 = res.__('emailList.invitation.intro3');
	replaceValue.text.eventTitle = event.title;
	replaceValue.text.eventDate = event.date_init;
	replaceValue.text.userFail = (userFail.name) ? EM.utils.getDisplayUsername(user) : userFail.email;

	sendOne('invitationDenied', replaceValue, function (err, responseStatus, html, text) {
		if(err)
			sails.log.error('email error:' + err + '|InvitationDenied|responseStatus:' + responseStatus);       
    })
};

/**
 * Generate Reset password link
 */
EM.dispatchForgotPasswordEmail = function(res, user, callback) {
	// note: receive user data from formulario NOT from DB !
	var replaceValue = EM.utils.i18n_htmlReplaceValue( user );
		replaceValue.email = user.email;
		replaceValue.subject = res.__('emailList.forgotPassword.subject');
		replaceValue.link.buttonLink = URLService.getResetPasswordLink( user );
		replaceValue.text.subject = replaceValue.subject;
		replaceValue.text.hi = res.__('emailList.forgotPassword.hi');
		replaceValue.text.text = res.__('emailList.forgotPassword.text');
		replaceValue.text.link = res.__('emailList.forgotPassword.link');


	sendOne('resetPasswordTempl', replaceValue, function (err, responseStatus, html, text) {
		if(err)
			sails.log.error('email error:' + err + '|ForgotPassword|responseStatus:' + responseStatus);       
     })
};

function sendOne(templateName, locals, fn) {
 	// make sure that we have an user email
	if (!locals.email) {
		return fn(EmailAddressRequiredError);
	}
	// make sure that we have a message
	if (!locals.subject) {
		return fn(EmailAddressRequiredError);
	}

 	emailTemplates(templatesDir, function (err, template) {
		if (err) {
			//console.log(err);
			return fn(err);
		}
		// Send a single email
		template(templateName, locals, function (err, html, text) {
			if (err) {
				//console.log(err);
				return fn(err);
			}

			smtpTransport.sendMail({
				from:			 'SportActiviti.es <sharesportevents@gmail.com>',
				to:				locals.email,
				subject:		locals.subject,
				text:			text,
				html:			html
			}, function (err, responseStatus) {
				console.log(err);
				console.log(responseStatus);
				console.log(locals.email);
				if (err) {
					return fn(err);
				}
				return fn(null, responseStatus.message, html, text);
			});			
   		});
	});
}



