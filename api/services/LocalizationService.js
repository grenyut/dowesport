var constants = require('../../constants.js');

exports.formatLocalization = function( localization ) {
	var loc = {};
	loc.city = (localization.city) ? localization.city : 'n/a';
	loc.country = (localization.country) ? localization.country : 'n/a';
	loc.formatted_address = (localization.formatted_address) ? localization.formatted_address : 'n/a';
	loc.name = (localization.name) ? localization.name : 'n/a';
	loc.lat = (localization.location.lat) ? localization.location.lat : undefined;
	loc.lon = (localization.location.lon) ? localization.location.lon : undefined;
	return loc;
}