var constants = require('../../constants.js'),
	validator = require('validator'),
	stopWordsES = require('stopwords').spanish,
	stopWordsEN = require('stopwords').english,
	Q = require('q');	


exports.formatIdTitle = function formatIdTitle(title){
	//TODO Test!
    for (var index in stopWordsES) {
        title = title.replace(new RegExp('\\b' + stopWordsES[index] + '\\b', "g"), '');
    }

    for (var index2 in stopWordsES) {
        title = title.replace(new RegExp('\\b' + stopWordsEN[index2] + '\\b', "g"), '');
    }    

    sails.log.warn('test the url stopwords filter!!!');
    sails.log.warn(title);

	title = StringService.removeURLReservedChars(StringService.removeAccents(title));
	title = title.replace(/\s{2,}/g,' ');
	title = title.replace(/^\s+/g,'').replace(/\s+$/g,'');
	return StringService.replaceAll(title, ' ', '-').toLowerCase();
};


//Localization attr already validated by place.checkExistence
exports.validateEventStep1 = function( data ) {
	var validDate = DateService.validDate( data.initDate );

	console.log('alpha title ' + StringService.isAlpha( data.title ));
	console.log('title ' + data.title );
	console.log('date ' + data.initDate + ' ' + validator.isDate(data.initDate))

		console.log(StringService.isAlpha( data.title )	);	
		console.log( validator.isNumeric( data.activity ));	
		console.log( DateService.validDate( data.initDate ));	
		console.log( DateService.isDateGreaterThanToday( DateService.getDate(data.initDate)  ));	
		console.log( validator.isIn(data.publicEvent, constants.eventIsPublicValues));	

	return validDate
		&& StringService.isAlpha( data.title )		
		&& validator.isNumeric( data.activity )
		&& DateService.isDateGreaterThanToday( DateService.getDate(data.initDate) )
		&& validator.isIn(data.publicEvent, constants.eventIsPublicValues);
}


exports.validateEventStep2 = function( data ) {
	var validadatePrice = true;;

		console.log('VALIDATING TOTAL-PARTICIPANTS!');
		console.log(data.total_participants);
		console.log(( (validator.isInt( data.total_participants ) && data.total_participants >= 0 ) || data.total_participants == '' ));
		console.log(( (validator.isInt( data.total_participants ) && data.total_participants >= 0 ) ));
		console.log( data.total_participants == '' );
		console.log( 'price ' + validator.isFloat( data.price ) + ' ' + data.price );
		console.log( 'free ' + validator.isIn( data.free, constants.eventIsFree ) + ' ' + data.free );
		console.log('END VALIDATING TOTAL-PARTICIPANTS!');

	//case where the event is not free, we format the price
	if( data.free == constants.eventIsFree[0] ) {
		console.log('format price!');

		data.price = data.price.replace(',', '.');
		validadatePrice = validator.isFloat( data.price );
	} else {
		data.price = 0;
	}


	return validator.isIn( data.difficulty, constants.eventDifficulties )
			&& ( (validator.isInt( data.total_participants ) && data.total_participants >= 0 ) || data.total_participants == '' )
			&& validadatePrice
			&& validator.isIn( data.free, constants.eventIsFree );
}


/** 
	arrayQuery = [
		id_event,
		constants.invitationNoResponse,
		constants.invitationAccepted
	]; 
**/
exports.getEventGuests = function getEventGuests( arrayQuery ) {
	var deferred = Q.defer();
	User.query('SELECT u.*, i.status as invitationStatus FROM ' + constants.dbName + '.users u \
				JOIN ' + constants.dbName + '.invitations i on u.email = i.email \
				AND i.id_event = ? \
				WHERE i.status IN (?,?) LIMIT 10', arrayQuery, 
		function( err, result ) {
			if(err) {
	    		deferred.reject( new Error( err ) ); 								
			} else {
				deferred.resolve(result);
			}
			return;							
		});			
 	return deferred.promise;	
}



