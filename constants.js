var path = require('path');

function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

/////IMAGE CONSTANT VALUES
define('maxNumUploadedImages', 8);

/////
/////IMAGE URL, EXTENSIONS...
define('saveUsersImgURI', '/assets/public/image/profile/');
define('relativeUsersImgURI', '/image/profile/');

define('saveEventsImgURI', '/assets/public/image/event/');
define('relativeEventsImgURI', '/image/event/');

define('typePNG', 'PNG');
define('imgExtension', '.png');
define('jpegExtension', '.jpeg');

define('baseEventImgName', 'eventImage');

define('bigImgExtension', '_big');
define('mediumImgExtension', '_medium');
define('smallImgExtension', '_small');
define('thumbImgExtension', '_thumb');


/////IMAGE SIZES//////
define('minProfileImgSize', '180');

define('bigImgEventWidth', '1024');
define('bigImgEventHeight', '683');
define('mediumImgEventWidth', '639');
define('mediumImgEventHeight', '426');

define('backgroundImageColor', '#EDEDED');

define('bigImgProfileSize', '480');
define('smallImgProfileSize', '240');
define('thumbImgProfileSize', '120');
//////END IMAGE SIZES///////


//define('avatarName', 'avatar_');

define('rootUrl', 'http://localhost:3000/');
define('staticUsersImgURI', '/profileImages/users/');



////// BD defaults ///////
define('dbName', 'shareSportEventsDBTest');

define('genderUndefined', 'undisclosed');

//Event status
define('eventPublished', '0');
define('eventDraft', '1');
define('eventIsPublicValues', [0, 1]);
define('eventIsFree', [0, 1]);

//Event status
define('publicEvent', '1');
define('privateEvent', '0');

define('eventDifficulties', [0, 1, 2, 3]);

//Invitation defaults
		//0->no response
		//1->accepted
		//2->denied
define('invitationNoResponse', '0');
define('invitationAccepted', '1');
define('invitationDenied', '2');

////// END BD defaults ///////

////// URLs ///////
define('invitationDetail', 'invitation/show/');
define('invitation', 'invitation/');
define('roomEventDetail', '/room/event/');
///// FI URLs /////


//Environments
define('prodEnv', 'production');
define('devEnv', 'development');

//Production variables
if(process.env.NODE_ENV === 'production') {
	define('rootUrl', 'http://localhost:1337/');	
	define('saveUsersImgURI', '/assets/profileImages/users/');
	define('staticUsersImgURI', '/profileImages/users/');
	define('dbName', 'shareSportEventsDBTest');

}