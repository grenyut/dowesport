define([
	'jqueryFileUpload'
	], function(jqueryFileUpload) {   

    var uploadImage = Backbone.View.extend({

        events: {
            'change .image': 'fileSelected'
            //'change #image0': 'fileSelected'
        },

        initialize: function( options ){
        	if( !options.destUrl ) throw 'File upload needs a destination url';        	
        	if( !options.idElem ) throw 'File upload needs a destination url';
        	
        	var idElem = options.idElem,
        		formParams = (options.formParams) ? options.formParams : {};

			// Initialize the jQuery File Upload plugin
			$( idElem ).fileupload({
				url: options.destUrl,
				formData: formParams,
			    // This element will accept file drag/drop uploading
			    //dropZone: $('#drop'),

			    // This function is called when a file is added to the queue;
			    // either via the browse button, or via drag/drop:
			    add: function (e, data) {

			        //var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
			        //    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

			        // Append the file name and file size
			        //tpl.find('p').text(data.files[0].name).append('<i>' + formatFileSize(data.files[0].size) + '</i>');
			        // Add the HTML to the UL element
			        //data.context = tpl.appendTo(ul);
			        // Initialize the knob plugin
			        //tpl.find('input').knob();
			        // Listen for clicks on the cancel icon
			        // tpl.find('span').click(function(){
			        //     if(tpl.hasClass('working')){
			        //         jqXHR.abort();
			        //     }
			        //     tpl.fadeOut(function(){
			        //         tpl.remove();
			        //     });
			        // });
			        // Automatically upload the file once it is added to the queue
			        var jqXHR = data.submit();
			    },
			    progress: function(e, data){
			        // Calculate the completion percentage of the upload
			        // var progress = parseInt(data.loaded / data.total * 100, 10);
			        // Update the hidden input field and trigger a change
			        // so that the jQuery knob plugin knows to update the dial
			        // data.context.find('input').val(progress).change();
			        // if(progress == 100){
			        //     data.context.removeClass('working');
			        // }
			    },
			    done: _.bind( this.successHandler, this ),
			    fail: _.bind( this.errorHandler, this ),
			});         	
        },

        fileSelected: function( e ) {
        	var idElem = e.currentTarget.id;
		},

		successHandler: function( e, response ) {
			this.trigger('imageUploaded', response.result, {id: e.target.getAttribute('id').slice(-1)});
		},

	 	errorHandler: function( e, response ) {
	 		this.trigger('errorImageUploaded', response.jqXHR.responseJSON, {id: e.target.getAttribute('id').slice(-1)} );
		},

		createNewImage: function(imgUrl, imgId) {
			newImg = document.createElement("img");
			newImg.src = imgUrl;
			newImg.id = imgId;
			return newImg;
		}
    });

    return uploadImage;
});



