define(['jquery'],
	function($) {
		return {
			parse: {
				getFormData: function getFormData($form) {
				    var unindexed_array = $form.serializeArray();
				    var indexed_array = {};

				    $.map(unindexed_array, function(n, i){
				        indexed_array[n['name']] = n['value'];
				    });

				    return indexed_array;
				}
			},
			validateEmail: function validateEmail(email){
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return filter.test(email);
			}
		};
	}
);
