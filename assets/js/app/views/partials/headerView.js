define(['backbone'], function(Backbone) {

	var headerView = Backbone.View.extend({

		el: '#header',

		events: {
			'click #registerLink': 'loadModal',
			'click #loginLink': 'loadModal'
		},

		currentModal: undefined, 

		loadingModal: false,

        /**
         * Initialize header
         * @param  {object} options
         * options.csrf
         */
        initialize: function(options) {
            this.options = options;
            Backbone.View.prototype.initialize.call(this, options);
        }, 			


		loadModal: function( e ) {
			var modalName = '', modalFormParams = {};
			if( !this.loadingModal ) {
				this.loadingModal = true;
				if (typeof e == 'string' || e instanceof String) {
					modalName = e;
				} else {
					e.preventDefault();
					modalName = e.currentTarget.getAttribute('data-id-modal');
				}
				//get params for next modal
				if( this.currentModal ){
					modalFormParams.formValues = {};
					_.extend( modalFormParams.formValues, this.currentModal.model.toJSON() );
				}
				
				this.showModal(modalName, modalFormParams);
			}		
		},

        /**
        * shows modal
        * @param  {object} options 
        * if options.redirect is false we don't want to redirect the app to the url send 
        * back by the server. We want to stay on the same page
        */			
		showModal: function(modalName, options) {
			var modalOptions = options || {};
			_.extend( modalOptions, this.options );
	        require(['app/views/modals/' + modalName + 'ModalView'], 
	        	_.bind( function ( ModalView ) {
		            this.currentModal = new ModalView( modalOptions );
		            this.currentModal.render();
		            this.currentModal.once( 'changeModal', this.loadModal, this);
	            	this.listenTo(this.currentModal, 'doBaseViewAction', this.doBaseViewAction );
	            	this.loadingModal = false;
		        }, this )
		    );						
		},

        doBaseViewAction: function() {
            this.trigger( 'doBaseViewAction' );
        },

	});
	return headerView;
});
