define([
    'backbone', 
    'underscore',
    'app/models/comments/commentModel',
    'tpl!templates/partials/commentTempl.html'
], function(Backbone, _, CommentModel, CommentTempl) {
    
    return Backbone.View.extend({

        el: '#commentsBlock',

       events: {
            'click #commentButton': 'saveComment',
        },   

        template: CommentTempl,//_.template(CommentTempl), 

        collection: new Backbone.Collection({ model: CommentModel }),

        initialize: function( options ) {
            if( !options.url ) throw new Error('missing the url to fetch comments');
            this.collection.url = 'comments';
            this.collection.fetch();
            this.listenTo(this.collection, 'add', this.render);
        },  

        //when the headerView shows the login modal and wants to 
        //continue with the default behaviour of the base view, this method
        //is executed.
        doBaseViewAction: function() {
            this.$el.find('#commentButton').click();
        },

        saveComment: function( e ) {
            this.collection.create({ message: this.$el.find('#commentArea').val() }, { wait: true });          
        },

        /**
         * Render HTML from this.template() into this.$el and notify components so they can attach with setElement()
         * @return {this}
         */
        render: function( comment ) {
            this.$el.find('#commentsBlockComments').append(this.template( comment.attributes ));
        }       
    });

});
