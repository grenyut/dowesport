define(['BaseView', 
        'app/views/widgets/autocomplete',
        'app/global/fileUpload/fileUpload',
        'parsley'
        ], function(BaseView, AutocompleteWidget, fileUpload, Parsley) {
    
    return BaseView.extend({
        el: '#editUserForm',

        parsleyForm: undefined,

        autocompleteComponent: undefined,

        // model: UserModel,

        events: {
            // 'submit form': 'submit',
            // 'keyUp': 'processKey'
        },   

        initialize: function(options) {
            BaseView.prototype.initialize.call(this, options);
            this.autocompleteComponent = new AutocompleteWidget( {type: 'political'} );
            new fileUpload( {
                el: '#imageSelector',
                destUrl: 'profileimage',
                idElem: '#avatar0',
                formParams: {
                    id: $('#id').val(),
                    _csrf: this.options.csrf
                }
            } );
            this.parsleyForm = this.$el.parsley();
        },  

        getComponentParamsToSubmit: function(){
            if( autocompleteComponent ) 
                return autocompleteComponent.getCountryAndCity();
            else return {};
        },

        render: function() {

        }
                   
    });
});
