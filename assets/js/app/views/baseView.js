define(['backbone', 
        'jquery', 
        'i18n!nls/baseViewBundle',
        'Utils', 
        'app/views/partials/headerView',
        'bootstrap',
], function(Backbone, $, Bundle, Utils, HeaderView) {
    
    var BaseView = function( options ) {
        //this.options has csrf attribute when it is a form
        this.options = {},
        this.header = undefined,
        Backbone.View.apply( this, [options] );
        this.bundle = Bundle;
    };

    _.extend(BaseView.prototype, Backbone.View.prototype, {
        /**
         * Initialize view
         * Subclasses must call "super" with something like BaseView.prototype.initialize.call(this, options)
         * @param  {object} options
         * options.csrf
         */
        initialize: function( options ) {
            this.options = options;
            this.headerComponent = new HeaderView( options );
            Backbone.View.prototype.initialize.call( this, options );
            this.listenTo( this.headerComponent, 'doBaseViewAction', this.doBaseViewAction );
        },  

        //Show the login modal and doesn't redirect to the redirectURL returned
        //by the server. It executes doBaseViewAction
        showLoginModal: function() {
            this.headerComponent.showModal( 'login', {redirect: false} );
        },

        displayErrorMessage: function( idElem, message ) {
        },

        //when the headerView shows the login modal and wants to 
        //continue with the default behaviour of the base view, this method
        //is executed.
        doBaseViewAction: function() {},

        /**
         * Render HTML from this.template() into this.$el and notify components so they can attach with setElement()
         * @return {this}
         */
        render: function() {
            var params = {
                bundle: bundle
            };

            params = _.extend(params, this.getRenderParameters());
            this.$el.html(this.template(params));
            return this;
        },

        /**
         * Get template parameters
         * By default return "model" or "collection" if available on this.
         * Override to add other parameters.
         * The "render" method already sets "bundle" and "components" parameters, be careful not to override those on accident.
         * @return {object} Template parameters object, with this.model.toJSON() as "model", or "collection" respectively
         */
        getRenderParameters: function() {},

        rendered: function() {},

        errorCallBack: function (xhr, status, data){},

        renderErrorModal: function ( errorCode, errorMessage){},

        /**
         * Bind callback() of this to this
         * @return {function} this.callback bound to this
         */
        bindCallback: function() {
            return _.bind(this.callback, this);
        },
        
        getComponentParamsToSubmit: function(){},

        processKey: function(e) { 
            if(e.which === 13) {// enter key
                this.submit(e);
            }
        }           
    });

    BaseView.extend = Backbone.View.extend;

    return BaseView;
});
