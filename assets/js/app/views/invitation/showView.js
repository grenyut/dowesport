define([
    'BaseView', 
    'app/models/invitationModel'
    ], function(BaseView, InvitationModel) {
    
    return BaseView.extend({
        lastTarget: undefined,

        model: new InvitationModel(),

        el: '.container',

        events: {
            'click #acceptInvitation': 'submit',
            'click #denyInvitation': 'submit'
        },   

        initialize: function( options ) {
            options.redirect = false;
            BaseView.prototype.initialize.call( this, options );
        }, 

        doBaseViewAction: function() {
            this.lastTarget.click();
        },               

        showLoginModal: function() {
            this.headerComponent.showModal( 'login', {
                redirect: false,
                formValues: {
                    email: this.$el.find('#email').val()
                }
            } );
        },          

        submit: function( e ) {
            e.preventDefault(); 
            this.lastTarget = e.currentTarget;
            var invitationStatus = e.currentTarget.getAttribute('data-status');
            this.model.save({
                id: this.$el.find('#idInvitation').val(),
                status: invitationStatus,
                _csrf: this.options.csrf
            }, {
                success: _.bind( this.model.successCallback, this.model ),
                error: _.bind( this.model.errorCallback, this.model )
            });          
        }  
                   
    });
});
