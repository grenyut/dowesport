define([
    'BaseView', 
    'parsley',
    'app/models/eventModel',
    'app/views/widgets/autocomplete',
    'app/views/widgets/locationPicker',
    'app/views/widgets/datePicker'
    ], function(BaseView, Parsley, EventModel, AutocompleteWidget, LocationPicker, DatePicker) {
    
    return BaseView.extend({

        model: new EventModel(),

        el: '#editEventForm',

        locationMap: undefined,

        localizationLabel: undefined,
        currentLabel: undefined,

        lastSubmitedButton: undefined,

        events: {
            'click #publishButton': 'submit',
            'click #saveDraftButton': 'submit',
            'keyUp': 'processKey',
            'click #localizationButton': 'openMap',
            'click #getLocalizationButton': 'updateLocationLabelModel'
        },   

        initialize: function initialize(options) {
            BaseView.prototype.initialize.call(this, options);
            var autoCompleteOptions = {
                el: '#localizationField'
            };
            this.localizationLabel = new AutocompleteWidget( autoCompleteOptions );
            this.listenTo( this.localizationLabel.model, 'change', this.localizationChanged );
            this.listenTo( this.localizationLabel, 'noGeolocationInfo', this.noGeolocationInfo );
            this.listenTo( this.model, 'showLoginModal', this.showLoginModal );

            new DatePicker({
                model: this.model,
                selectors: '.form_datetime',
                additionalOptions: {
                    linkField: 'dateTime',
                    linkFormat: 'dd/mm/yyyy hh:ii'
                }
            });  
            //autoCompleteOptions = null; 
        },  

        getComponentParamsToSubmit: function getComponentParamsToSubmit(){
            if( autocompleteComponent ) 
                return autocompleteComponent.getCountryAndCity();
            else return {};
        },        

        openMap: function openMap( e ) {
            var component, placeResult = undefined,
                idCurrentTarget = e.currentTarget.getAttribute('id');

            // if( $(e.currentTarget).data('input-id') === 'localizationField' ) {
            this.currentLabel = this.localizationLabel;
            // }
            // else {
            //     this.currentLabel = this.meetingPointLabel;
            // }

            placeResult = this.currentLabel.model;

            if ( !this.locationMap ) {
                this.locationMap = new LocationPicker( placeResult );
                this.listenTo(this.locationMap, 'mapCenterChanged', this.updateLabelModel );
            } else {
                this.locationMap.render();
            }
        },

        updateMap: function updateMap( labelModel ) {
            var that;
            if ( this.locationMap ) {
                that = this;
                this.locationMap.setValues( labelModel ).done( function(){
                    that.locationMap.updateMap();
                });
            }  
        },

        updateLabelModel: function updateLabelModel( e ) {
            this.currentLabel.model.set( this.locationMap.getMapInfo() );
        },

        localizationChanged: function localizationChanged() {
            this.updateMap(this.localizationLabel.model);
        },

        //autocompleWidget has not found a location so we show an error message and the map lo localize it
        noGeolocationInfo: function noGeolocationInfo( e ) {
            var localizationField = this.$el.find('#localizationField').parsley();
            window.ParsleyUI.removeError(localizationField, "noGeolocationInfo");
            window.ParsleyUI.addError(localizationField, "noGeolocationInfo", this.bundle.ERROR_GEO_FIELD);
            this.$el.find('#localizationButton').click();
        },

        doBaseViewAction: function doBaseViewAction() {
            this.$el.find('#' + this.lastSubmitedButton).click();
        },

        submit: function submit( e ) {
            var params;
            e.preventDefault(); 
            if( document.activeElement.id != 'localizationField' ) {
                //Validation
                var parsleyOptions = {
                    "excluded": "input[type=hidden], [disabled]"
                }
                if( !this.$el.parsley(parsleyOptions).validate() ) return;
                if( !this.localizationLabel.hasChanged() ) {
                    this.noGeolocationInfo();
                    return;
                }
                //only used when we are editing not creating an event
                if( this.numImages != 'undefined' && this.numImages === 0 ) { 
                    alert('sube una imagen! TODO validation');
                    return;
                }                
                //END Validation

                //save the last submited button to do the same action if the user 
                //is not loged or registered
                this.lastSubmitedButton = e.currentTarget.getAttribute('id');

                params = this.getFormParams();
                params.status = e.currentTarget.getAttribute('data-status-id');
                this.model.save( params, {
                    success: _.bind( this.model.successCallback, this.model ),
                    error: _.bind( this.model.errorCallback, this.model )
                });
                params = null;
            }
        },       

        getFormParams: function getFormParams(){
            var params = {
                title: this.$el.find('#title').val(),
                description: this.$el.find('#description').val(),
                initDate: this.$el.find('#dateTime').val(),
                activity: $('#activityList').find('.active').first().data('activity-id'),
                publicEvent: this.$el.find("input[name='publicEvent']:checked").val(),
                _csrf: this.options.csrf
            };
            if ( this.localizationLabel ) {
                params.localization = this.localizationLabel.getGeolocationInfo();
            }
            return params;
        }        
    });
});
