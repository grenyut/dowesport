define([
    'app/global/fileUpload/fileUpload',
    'app/models/baseModel',
    'app/views/event/newView',
   	'app/views/widgets/autocomplete'
    ], function(fileUpload, BaseModel, NewView, AutocompleteWidget) {
    
    return NewView.extend({

        events: function(){
            return _.extend({},NewView.prototype.events,{
                'click .dropdown-menu li a' : 'currencySelection',
                'click input[name=price]:checked': 'feeSelection',
                'click .deleteImage': 'deleteImage',
            });
        },

        imgCollection: new Backbone.Collection( window.INITIAL_DATA.imgCol, { model: BaseModel } ),
        fileUpload: undefined,
        numImages: window.INITIAL_DATA.numImages,
        idEvent: window.INITIAL_DATA.id_event,
        _csrf: window.INITIAL_DATA._csrf,

        initialize: function init(options) {
            NewView.prototype.initialize.call(this, options);

            this.model.set({ id: this.idEvent });

            var locParams = {
	            city: this.$el.find( '#localization_city' ).val(),
	            country: this.$el.find( '#localization_country' ).val(),
	            formatted_address: this.$el.find( '#localization_formatted_address' ).val(),
	            name: this.$el.find( '#localization_name' ).val(),
	            location: {
	                lat: this.$el.find( '#localization_lat' ).val(),
	                lon: this.$el.find( '#localization_lon' ).val()
	            }
            };
            this.localizationLabel.model.set( locParams, {silent: true} );

            this.fileUpload = new fileUpload({
                el: '#imageSelector',
                destUrl: '/event/' + this.idEvent + '/image',
                idElem: '#image0, #image1, #image2, #image3',
                formParams: {
                    id: this.idEvent,
                    _csrf: this._csrf
                }
            });      

            this.listenTo( this.fileUpload, 'imageUploaded', this.imageUploaded );       
            this.listenTo( this.fileUpload, 'errorImageUploaded', this.errorImageUploaded );  

            this.imgCollection.url = '/event/' + this.idEvent + '/image';
            this.listenTo(this.imgCollection, 'add', this.renderNewImage);
            this.listenTo(this.imgCollection, 'destroy', this.imageDeleted);
            this.listenTo(this.imgCollection, 'err500', this.errorImageDeleted);
        },  

        getFormParams: function gFP(){
        	var params = NewView.prototype.getFormParams.call(this);
            _.extend( params, {
                total_participants: this.$el.find('#numParticipants').val(),
                duration: this.$el.find('#duration').val(),
                equipment: this.$el.find('#equipment').val(),
                difficulty: this.$el.find("input[name='difficulty']:checked").val(),
                transport: this.$el.find('#transport').val(),
                meetingPoint: this.$el.find('#meeting_point').val(),
                free: this.$el.find('input[name=price]:checked').val(),
                price: this.$el.find('#priceValue').val(),
            }) 
            return params;
        },

        imageUploaded: function iU(result, data){
            this.imgCollection.add(result, {at: data.id});
            this.numImages++;
        },

        renderNewImage: function rNI( model ){
            //TODO Move the template to an html template!! Careful
            // with the html inside the edit.ejs, it must be the same!
            var idElem = this.imgCollection.indexOf( model ),
                imgDiv = this.$el.find( '#imageCol' + idElem ),
                template = ['<div class="thumbnail">',
                                '<img src="<%=data.url%>" id="outputImage<%=index%>">',
                                '<div class="caption">',
                                    '<p><a href="#" class="btn btn-default deleteImage" data-id="<%=data.id%>" role="button">Borrar (hardcoded)</a></p>',
                                '</div>',
                            '</div>'],
                compiled = _.template(template.join(''));

            imgDiv.html( compiled({data: model.toJSON(), index: idElem}) );
        },

        deleteImage: function dI( e ) {
            e.preventDefault();
            var imgId = e.currentTarget.getAttribute("data-id"), domImgPosition, model;
            model = this.imgCollection.get( imgId );
            domImgPosition = this.imgCollection.indexOf( model );
            model.destroy({ 
                wait: true, 
                error: _.bind(model.errorCallback, model),
                domImgPosition: domImgPosition 
            });          
        },

        imageDeleted: function iD(model, collection, options){
            //TODO Move the template to an html template!! Careful
            // with the html inside the edit.ejs, it must be the same!            
            var idElem = options.domImgPosition,
                imgDiv = this.$el.find( '#imageCol' + idElem );
                //template = '<input type="file" id="image<%=i%>" class="image" name="eventImage">',
                //compiled = _.template(template);
            console.log('adding another input file doesn\'t work. It would need to bind another fileUpload. The best is to hide and show the element.');    
            imgDiv.empty();
        },        

        errorImageUploaded: function eIU(responseMsg, data){
            //TODO logic
            console.log('errorImageUploaded: ' + JSON.stringify(responseMsg));
        },

        errorImageDeleted: function eID(responseMsg){
            //TODO logic
            console.log('errorImageDeleted: ' + JSON.stringify(responseMsg));
        },        

        currencySelection: function cS( e ) {
            e.preventDefault();
            this.$el.find('#currency').html(e.currentTarget.innerHTML);
        },

        feeSelection: function fS( e ) {
            var required, disabled,
                priceInput = this.$el.find('#priceValue');
            if(e.currentTarget.value == 0) {
                required = 'true';
                disabled = false;
            } else {            
                required = 'false';   
                disabled = true; 
            } 
            priceInput.attr('data-parsley-required', required);
            priceInput.attr('disabled', disabled);
            this.$el.parsley().destroy();
        }
                   
    });
});
