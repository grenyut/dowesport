define([
    'BaseView',
    'app/views/partials/commentView'
    ], function(BaseView, CommentView) {
    
    return BaseView.extend({

        el: '#eventView',

        commentView: undefined,

        events: {
        },   

        initialize: function(options) {
            var commentOptions = {};
            BaseView.prototype.initialize.call(this, options);
            commentOptions.url = 'comments';
            this.commentView = new CommentView( commentOptions );
        },  

        doBaseViewAction: function() {
            this.commentView.doBaseViewAction();
        }        
    });
});
