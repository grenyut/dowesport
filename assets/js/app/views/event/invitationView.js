define([
    'BaseView', 
    'Utils',
    'app/models/invitationModel',
    'tagsInput',
    ], function(BaseView, Utils, ShareModel, TagsInput) {
    
    return BaseView.extend({

        model: new ShareModel(),

        el: '#invitationForm',

        events: {
            'click #shareButton': 'submit',
            'itemAdded #emailTags': 'itemAdded'
        },   

        initialize: function( options ) {
            BaseView.prototype.initialize.call( this, options );
            this.$el.find('#emailTags').tagsinput( {confirmKeys: [13, 188, 32]} );
        },  

        itemAdded: function(event) {
            if(! Utils.validateEmail(event.item) ) {
                this.$el.find('#emailTags').tagsinput('remove', event.item);
            }
        },

        submit: function( e ) {
            e.preventDefault(); 
            this.model.save({
                id_event: this.$el.find('#id_event').val(),
                emailList: this.$el.find('#emailTags').val(),
                _csrf: this.options.csrf
            }, {
                success: _.bind( this.model.successCallback, this.model ),
                error: _.bind( this.model.errorCallback, this.model )
            });
        }        
                   
    });
});
