define([
	'app/views/modals/modalView',
	'app/models/loginModel',
	'i18n!nls/headerBundle',
	'tpl!templates/partials/loginTempl.html'
	],
	function(ModalView, LoginModel, bundle, template) {

		var loginModalView = ModalView.extend({
			id: 'loginModal',	

			model: new LoginModel(),

			bundle: bundle,

			template: template,	

			render: function() {
				if( this.options.formValues && this.options.formValues.email ) {
					this.model.set( {email: this.options.formValues.email} );
				}					
				ModalView.prototype.render.call(this);
				this.parsleyForm = this.$el.find('#loginForm').parsley();			
			}
		});

		return loginModalView;
	}
);
