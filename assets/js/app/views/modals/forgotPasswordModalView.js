define([
	'app/views/modals/modalView',
	'app/models/forgotPasswordModel',	
	'i18n!nls/headerBundle',
	'tpl!templates/partials/passwordTempl.html'
	],
	function(ModalView, ForgotPasswordModel, bundle, template) {

		var passwordModalView = ModalView.extend({
			id: 'passwordModal',	

			model: new ForgotPasswordModel(),

			bundle: bundle,

			template: template,	

	        initialize: function(options) {
	            this.options = options;
	            Backbone.View.prototype.initialize.call(this, options);
	            // this.listenTo(this.model, 'doBaseViewAction', this.doBaseViewAction )
	        }, 			

			render: function() {
				ModalView.prototype.render.call(this);
				this.parsleyForm = this.$el.find('#passwordForm').parsley();
			}
		});

		return passwordModalView;
	}
);
