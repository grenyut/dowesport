define([
	'app/views/modals/modalView',
	'app/views/widgets/autocomplete',
	'app/models/registerModel',	
	'i18n!nls/headerBundle',
	'tpl!templates/partials/registerTempl.html'
	],
	function(ModalView, AutocompleteWidget, RegisterModel, bundle, template) {

		var registerModalView = ModalView.extend({
			localizationLabel: undefined,

			id: 'registerModal',

			model: new RegisterModel(),

			bundle: bundle,

			template: template,

	        initialize: function( options ) {
	            ModalView.prototype.initialize.call(this, options);
	        },  			

			render: function() {
				if( this.options.formValues && this.options.formValues.email ) {
					this.model.set( {email: this.options.formValues.email} );
				}						
				ModalView.prototype.render.call(this);
				this.parsleyForm = this.$el.find('#registerForm').parsley();			
			},

			initAutocomplete: function() {
	            var autoCompleteOptions = {
	            	el: '#localizationModalField',
					restrictions: {
						types: ['(cities)']
					}
 				};
            	this.localizationLabel = new AutocompleteWidget( autoCompleteOptions );
            	this.listenTo( this.localizationLabel.model, 'change', this.localizationChanged );
            	this.listenTo( this.localizationLabel, 'noGeolocationInfo', this.noGeolocationInfo );	
			},

	        localizationChanged: function() {
	        	var model = this.localizationLabel.model;
	        	this.$el.find( '#localization_city' ).val( model.get( 'city' ) );
	        	this.$el.find( '#localization_country' ).val( model.get( 'country' ) );
	        	this.$el.find( '#localization_formatted_address' ).val( model.get( 'formatted_address' ) );
	        	if( model.get( 'location' ) ) {
		        	this.$el.find( '#localization_lat' ).val( model.get( 'location' ).lat );
		        	this.$el.find( '#localization_lon' ).val( model.get( 'location' ).lon );
		        	//Clean the localizationField error message
		        	var localizationField = this.$el.find('#localizationModalField').parsley();
		        	window.ParsleyUI.removeError(localizationField, "noGeolocationInfo");
	        	}
	        },

	        noGeolocationInfo: function() {
	        	this.localizationLabel.model.clear();
	        	this.$el.find('#localizationModalField').val('');
	        },

	        submit: function( e ) {
	            e.preventDefault();
	            if( document.activeElement.id != 'localizationModalField' ) {
		            if( this.validateForm() ) {
		            	ModalView.prototype.submit.call( this, e );
		            }
		        }
	        },

	        //Validate that we have a city informed. If not, validate all the fields
	        //and show an error message for the localizationField
	        validateForm: function(){
	        	var city = this.localizationLabel.model.get( 'city' );
	            if( city && city !== 'n/a' ) {
	            	return true;
	            } else {
		            var localizationField = this.$el.find('#localizationModalField').parsley();
		            window.ParsleyUI.removeError(localizationField, "noGeolocationInfo");
		            window.ParsleyUI.addError(localizationField, "noGeolocationInfo", this.bundle.ERROR_FIELD);
		            this.$el.find('form').parsley().validate();
		            return false;
	            }
	        }
		});

		return registerModalView;
	}
);
