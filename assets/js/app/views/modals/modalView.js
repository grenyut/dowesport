define(['backbone',
        'Utils', 
        'parsley',
        'i18n!nls/headerBundle'
    ], function(Backbone, Utils, Parsley, Bundle) {
    
    return Backbone.View.extend({
        className: 'modal fade',

        bundle: Bundle,
        
        attributes: {
            tabindex: '-1',
            role: 'dialog',
            'aria-labelledby': 'myModalLabel',
            'aria-hidden': 'true'
        },

        parsleyForm: undefined,

        events: {
            'hidden.bs.modal': 'closeModal',
            'click #registerLink': 'loadNextModal',
            'click #loginLink': 'loadNextModal',
            'click #passwordModalLink': 'loadNextModal',
            'submit form': 'submit',
            'keyUp': 'processKey'
        },   

        /**
        * initializes modal
        * @param  {object} options
        * options.csrf
        * options.redirect: if false, we don't want to redirect the app to the url send 
        * back by the server. We want to stay on the same page
        */  
        initialize: function(options) {
            this.options = options;
            Backbone.View.prototype.initialize.call(this, options);
            this.listenTo( this.model, 'doBaseViewAction', this.doBaseViewAction );
        },         

        render: function() {
            var params = {
                bundle: this.bundle,
                _csrf: this.options.csrf
            };
            _.extend( params, this.model.toJSON() );
            this.$el.html( this.template( params) ).modal( 'show' );
            this.$el.on('shown.bs.modal', _.bind( this.initAutocomplete, this ))
        },

        hideModal: function() {
            this.$el.modal( 'hide' );
        },

        initAutocomplete: function() {},

        closeModal: function() {
            this.undelegateEvents();
            this.remove();
        },        

        loadNextModal: function( ev ) {
            ev.preventDefault();
            var modalName = ev.currentTarget.getAttribute('data-id-modal');
            this.$el.modal( 'hide' ).on('hidden.bs.modal', _.bind(function (e) {
              this.trigger('changeModal', modalName); 
            }, this));                              
        },

        submit: function( e ) {
            e.preventDefault();

            var formId = this.id.replace( 'Modal', 'Form' );
            var $form = $('#' + formId);
            
            if( this.$el.find('form').parsley().validate() ) { 
                var modelParams = Utils.parse.getFormData($form);
                modelParams._csrf = this.options.csrf;
                if ( this.options.hasOwnProperty('redirect') ) {
                    modelParams.redirect = this.options.redirect;                    
                }
                this.model.save(modelParams, { 
                    success: _.bind( this.model.successCallback, this.model ),
                    error: _.bind( this.model.errorCallback , this.model ) });
            }
        }, 

        doBaseViewAction: function() {
            this.trigger( 'doBaseViewAction' );
        },

        processKey: function(e) { 
            if(e.which === 13) {// enter key
                this.submit();
            }
        }                      
    });
});
