define([
	'BaseView',
    'jquery', 
    'underscore',
	'app/models/sport/sportModel',
    'app/models/sport/sportCollection'
], 
function(BaseView, $, _, SportModel, SportCollection) {
    
    return BaseView.extend({
        el: '#sportsPanel',

        classSportChecked: 'list-group-item-success',
        classLoading: 'list-group-item-warning',

        sportCollection: new Backbone.Collection( window.INITIAL_DATA, { model: SportModel } ),
        userCollection: new SportCollection( window.INITIAL_DATA, { model: SportModel, parse: true }),

        events: {
            'click #currentSportList': 'updateUserInterests',
            'keyup #sportSearch': 'updateSearchField'
        },   

        initialize: function(options) {
            BaseView.prototype.initialize.call(this, options);
            this.userCollection.url = window.location.pathname;

            this.listenTo(this.userCollection, 'sync', this.renderUserSport);
            this.listenTo(this.userCollection, 'destroy', this.removeUserSport);
            //this.listenTo(this.userCollection, 'remove', this.removeUserSport);
            // this.template = _.template($( "script#sportList" ).html());
            // this.templateActivityListItem = _.template($( "script#activityListElem" ).html());
        },  

        renderUserSport: function( userSport ) {
            this.$el.find( '#' + userSport.get('id') ).removeClass( this.classLoading );            
            this.$el.find( '#' + userSport.get('id') ).addClass( this.classSportChecked );
        },

        removeUserSport: function( userSport ) {
            this.$el.find( '#' + userSport.get('id') ).removeClass( this.classLoading );                        
            this.$el.find( '#' + userSport.get('id') ).removeClass( this.classSportChecked );
        },        

        updateSearchField: function( e ) {
            e.preventDefault();
            var value = e.currentTarget.value.toLowerCase() ,
                valueNotEmpty = (value != '') ? true : false;
                domElem = this.$el.find('#currentSportList'),//document.getElementById('currentSportList'),
                domElemParent = domElem.parent(),
                warningClass = 'list-group-item-info';

            //Out-of-the-flow DOM Manipulation
            domElem.detach();
            this.sportCollection.each(function( sport, index, list ) {
                    var idElem = '#' + sport.get('id');
                    if ( valueNotEmpty && sport.get("name").toLowerCase().indexOf( value ) > -1 ) {
                        domElem.find( idElem ).addClass( warningClass );
                    } else {
                        domElem.find( idElem ).removeClass( warningClass );
                    }
            });
            domElemParent.append( domElem );
            domElem = null;
            domElemParent = null;
        },  

        updateUserInterests: function( e ) {
            var id = e.target.getAttribute('id'),
                target = $(e.target),
                isPref = target.hasClass( this.classSportChecked ),
                isLoading = target.hasClass( this.classLoading ),
                model;
            if( !isLoading ) {
                if( !isPref ) {
                    //target.addClass( sportLoading ); //sport selected
                    this.addSport(  this.sportCollection.get( id ) );
                } else {
                    target.removeClass( this.classSportChecked ); //sport unselected
                    this.removeSport(  this.userCollection.get( id ) );
                }
                target.addClass( this.classLoading );
            }
            target = null;
            e.preventDefault();
        },

        addSport: function( sport ) {
            this.userCollection.create( sport.toJSON(), {wait: true} );
        },  

        removeSport: function( sport ) {
            sport.destroy({ wait: true });
        },                     
                   
    });
});
