define(['BaseView', './locationModel'], function(BaseView, LocationModel) {
    
    return Backbone.View.extend({

        el: '#mapModal',

        attributes: {
            style: 'width:100%; height:500px'
        },

        model: new LocationModel(),

        viewport: undefined,
        location: undefined,
        map: undefined,
        geocoder: undefined,

        mapOptions: {
            mapTypeId: 'roadmap',
            zoom: 14,
            streetViewControl: false
        },

        //We don't initialize it until the user needs it
        initialize: function( placeResult ) {
            this.setValues( placeResult )
                .done( 
                    _.bind( this.initializeMap, this ) 
                ).fail(function(err){
                    //TODO locationPicker fail when the user geolocation
                    // cannot be resolved
                });            
        },  

        initializeMap: function() {
            this.mapOptions.center = this.getMapLatLng();
            this.geocoder = new google.maps.Geocoder();
            this.map = new google.maps.Map( this.$el.find('#mapCanvas')[0], this.mapOptions ); 
            
            google.maps.event.addListener(this.map, 'dragend', _.bind( this.mapCenterChanged, this ));
            
            $('<div/>').addClass('centerMarker').appendTo( this.map.getDiv() )
                .click(function(){
                //do something onclick
                });   

            // Resize map to show on a Bootstrap's modal
            this.$el.on('shown.bs.modal', _.bind( this.resizeMap, this ));  
            this.render();                   
        },        

        render: function() {
            this.$el.modal( 'show' );
        },

        resizeMap: function() {
            google.maps.event.trigger(this.map, "resize");
            this.updateMap();
        },

        updateMap: function() {
            var currentCenter = this.getMapLatLng();  // Get current center before resizing
            this.map.setCenter(currentCenter); // Re-set previous center
        },

        mapCenterChanged: function( event ) {
            var these = this,
                cityAndCountry = undefined,
                location = undefined,
                center = these.map.getCenter();  

            location = {
                lat: center.lat(),
                lon: center.lng()
            };

            this.geocoder.geocode({
                'latLng': center
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if ( results[0] ) {
                        cityAndCountry = these.model.getCityAndCountry( results[0] );
                        these.model.set({
                            city: cityAndCountry.city,
                            country: cityAndCountry.country,
                            location: location
                        });
                    }
                } else {          
                    these.model.clear({ silent: true });            
                    these.model.set({
                        location: location
                    });
                }
                these.trigger('mapCenterChanged'); 
            });                     
        },

        getMapLatLng: function() {
            var modelLoc = this.model.get( 'location' );
            return new google.maps.LatLng( modelLoc.lat, modelLoc.lon );          
        },

        setValues: function( placeResult ) {
            var deferred = $.Deferred();
            this.viewport = undefined;
            this.location = undefined;
            if( placeResult.get( 'location' ).lat && placeResult.get( 'location' ).lon ) {
                this.model.set({
                    location: placeResult.get( 'location' )
                });                
                deferred.resolve();
            }

            var modelLoc = this.model.get( 'location' );

            if( !modelLoc.lat && !modelLoc.lon ) {
                if ("geolocation" in navigator) {
                    navigator.geolocation.getCurrentPosition(
                        _.bind( this.success, this, deferred ), 
                        _.bind( this.error, this, deferred ), 
                        {timeout: 5000});
                }
            }

            return deferred.promise();  
        },
        //We don't return name and formatted address info 
        //because it could be 
        getMapInfo: function() {
            var info = this.model.toJSON();
            return {
                city: info.city,
                country: info.country,
                location: {
                    lat: info.location.lat,
                    lon: info.location.lon
                }                
            }
        },

        success: function( deferred, pos ){
            this.model.set({
                location: {
                    lat: pos.coords.latitude,
                    lon: pos.coords.longitude
                }
            });              
            deferred.resolve();
        },

        error: function( deferred, err ){
            console.warn('ERROR(' + err.code + '): ' + err.message);
            deferred.fail();
        }                  
    });
});
