define(['./locationModel'], function(LocationModel) {   

    var autocompleteElem = Backbone.View.extend({
        input: undefined,

        el: '#searchTextField',

        initialize: function( options ){
            var restrictions = options.restrictions ? options.restrictions : {};
            //this.el = options.el ? options.el : this.el;
            this.model = new LocationModel();
            this.input = new google.maps.places.Autocomplete(this.el, restrictions);
            google.maps.event.addListener(this.input, 'place_changed', _.bind(this.activate, this));
        },

        activate: function( e ) {
            var inputValue, place;
            if( this.input.getPlace().geometry ) {
                inputValue = this.$el.val();
                place = this.input.getPlace();
                place.name = inputValue;
                this.model.set( this.model.parse( place ) );
            } else {
                this.trigger('noGeolocationInfo');
            }
        },

        getGeolocationInfo: function() {
            return this.model.toJSON();
        },

        hasChanged: function() {
            return this.model.hasChanged();
        }
    });

    return autocompleteElem;
});
