define(['datePickerLib'], 
    function(DatePickerLib) {   

        var datePicker = Backbone.View.extend({
            input: undefined,

            defaultOptions: {
                autoclose: true,
                format: 'dd/mm/yyyy hh:ii',
                minuteStep: 15,
                todayHighlight: true,
                startDate: new Date()
            },

            initialize: function( options ){
                var additionalOptions = options.additionalOptions ? options.additionalOptions : {};
                if( !options.selectors ) 
                    throw new Error('no datepicker selector choosed');
                $( options.selectors ).datetimepicker( 
                    _.extend( this.defaultOptions, additionalOptions)
                ).on( 'changeDate', _.bind(this.setDate, this));
            },

            setDate: function( e ) {
                this.model.set({'initDate': e.date});
            }
        });

        return datePicker;
});
