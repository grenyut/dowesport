//https://developers.google.com/maps/documentation/javascript/places?hl=es#place_details_results
define([], function() {   

    var locationModel = Backbone.Model.extend({
        
        defaults: {
            city: 'n/a',
            country: 'n/a',
            country_code: 'n/a',
            formatted_address: '',
            name: '',
            location: {
                lat: undefined,
                lon: undefined
            }
        },

        // initialize: function(){
        //     alert("Welcome to this world");
        // },

        parse: function( placeResult ) {
            var values = undefined, 
                cityAndCountry = {};

            if (!placeResult.geometry) return;

            cityAndCountry = this.getCityAndCountry( placeResult );

            return {
                id: placeResult.place_id,
                city: cityAndCountry.city,
                country: cityAndCountry.country,
                country_code: cityAndCountry.country_code,
                formatted_address: placeResult.formatted_address,
                name: placeResult.name,
                location: {
                    lat: placeResult.geometry.location.lat(),
                    lon: placeResult.geometry.location.lng()
                }
            };      
        },

        getCityAndCountry: function( placeResult ) {
            var result = {};
            _.each( placeResult.address_components, function( component, key, list ) {
                if( _.indexOf(component.types, 'locality') >- 1 && _.indexOf(component.types, 'political') >- 1 ){
                    result.city = component.long_name;
                }
                if( _.indexOf(component.types, 'country') >- 1 && _.indexOf(component.types, 'political') >- 1 ){
                    result.country = component.long_name;
                    result.country_code = component.short_name;
                }   
            });
            return result;
        }

    });

    return locationModel;
});
