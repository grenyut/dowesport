define([
	'BaseView'
	],
	function(BaseView) {

		var homeView = BaseView.extend({

			$el: $('#'),
			className: '',

			events: {
				// 'click #lost-password': 'lostPassword',
				// 'click #new-client': 'newClient'
			},

			initialize: function( options ) {
				BaseView.prototype.initialize.call(this, options);
			}
		});
		return homeView;
	}
);
