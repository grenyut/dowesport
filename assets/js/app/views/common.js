require.config({
    baseUrl: '/js',
    // urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        i18n: 'libs/require/i18n',
        jquery: 'libs/jquery/jquery-min',
        jqueryFileUpload: 'libs/jquery/jquery.fileupload',
        jqueryUiWidget: 'libs/jquery/jquery.ui.widget',
        //jqueryForm: 'libs/jquery/jquery.form',
        bootstrap: 'libs/bootstrap/bootstrap.min',
        datePickerLib: 'libs/bootstrap/bootstrap-datetimepicker.min',
        datePickerEs: 'libs/bootstrap/locales/bootstrap-datetimepicker.es',
        tagsInput: 'libs/bootstrap/bootstrap-tagsinput.min',        
        underscore: 'libs/underscore/underscore-min',
        backbone: 'libs/backbone/backbone-min',
        nls: 'nls',
        text: 'libs/require/text',
        templates: '../templates',
        parsley: 'libs/parsley/parsley-min',
        tpl: 'libs/underscore/underscore-tpl',
        BaseView: 'app/views/baseView',
        Utils: 'app/global/utils'
    },

    shim: {
        'underscore': { exports: '_' },
        'jquery': { exports: '$' },
        'backbone': {
            deps: ['jquery', 'underscore'],
            exports: 'Backbone'
        },
        'jqueryUiWidget': { deps: ['jquery'] },
        'jqueryFileUpload': { deps: ['jquery', 'jqueryUiWidget'] },
        'parsley': { deps: ['jquery'] },
        //http://stackoverflow.com/questions/13377373/shim-twitter-bootstrap-for-requirejs
        'bootstrap': { deps: ['jquery'] },
        'datePickerLib': { deps: ['jquery'] },
        'datePickerEs': { deps: ['datePickerLib'] },   
        'tagsInput': { deps: ['jquery', 'bootstrap'] },
        'Utils': { deps: ['jquery'] }
    },
});


//We include now our files
require([
    'jquery',
    'underscore',
    'backbone',
    'parsley',
    'tpl',
    'text',
    'i18n',
    'bootstrap'
], function() {
    // _.templateSettings = {
    //     evaluate: /\{\{(.+?)\}\}/g,
    //     interpolate: /\{\{=(.+?)\}\}/g
    // };
    
    require([$('#currentSection').val()], function(View){
      new View( {csrf: $('#csrf').val() } );
    });    
});