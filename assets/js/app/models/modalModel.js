define(['backbone'],
	function(Backbone) {

		var modalModel = Backbone.Model.extend({
			url: '',

			defaults: {
				redirect: true,
				_csrf: ''
			},

			successCallback: function(model, response) {
				if( this.attributes.redirect ) {
					location.href = response.redirectUrl;
				} else {
					this.trigger( 'doBaseViewAction' );
				}
			},

            errorCallback: function(model, response) {
                var notice = $( '#notice' );
                notice.html(response.responseJSON.msg); 
                notice.css( 'display', 'block' );
            }
		});
		return modalModel;
	}
);
