define(['app/models/modalModel'],
	function(ModalModel) {

		var registerModel = ModalModel.extend({
			url: '/user',

			defaults: {
				name: '',
				email: '',
				password: '',
				confirmation: '',
			}
		});

		_.extend(registerModel.prototype.defaults, ModalModel.prototype.defaults);

		return registerModel;
	}
);
