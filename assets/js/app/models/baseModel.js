define(['backbone'],
	function(Backbone) {
		var baseModel = Backbone.Model.extend({
			defaults: {
				_csrf: ''
			},

			successCallback: function(model, response) {
				if( response.redirect ) {
					location.href = response.redirect;
				}
			},

            errorCallback: function(model, response, options) {
            	var errCode = response.status;
                if( errCode == 401 ) {
                	this.trigger( 'showLoginModal' );
                } else if( errCode == 500 ) {
                	this.trigger( 'err500', response.responseJSON );
                }            	
            }
		});
		return baseModel;
	}
);
