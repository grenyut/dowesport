define(['app/models/baseModel'],
	function(BaseModel) {

		var invitationModel = BaseModel.extend({
			url: function() {
				return '/invitation';
			},

			defaults: function(){
				return _.extend({}, BaseModel.prototype.defaults, 
				{	
					emailList: [],
					id_event: undefined,
					status: undefined
				});
			}		
		});
		return invitationModel;
	}
);
