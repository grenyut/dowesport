define(['app/models/baseModel'],
	function(BaseModel) {

		var eventModel = BaseModel.extend({
			url: '/events',
			idAttribute: "id",

			defaults: function(){
				return _.extend({}, BaseModel.prototype.defaults, 
				{
					id: undefined,
					title: undefined,
					initDate: undefined,
					localization: {
			            city: 'n/a',
			            country: 'n/a',
			            formatted_address: '',
			            name: '',
			            location: {
			                lat: undefined,
			                lon: undefined
			            }
					},
					activity: undefined,
					status: undefined,
					meetingPoint: undefined,
					type: undefined,
					difficulty: undefined,
					publicEvent: 1,
					total_participants: undefined,
					free: 1, //1 free, 0 paying
					price: 0
				});
			}
		});
		return eventModel;
	}
);
