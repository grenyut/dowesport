define(['app/models/modalModel'],
	function(ModalModel) {

		var forgotPasswordModel = ModalModel.extend({
			url: '/forgot_password',

			defaults: {
				email: '',
			}
		});

		_.extend(forgotPasswordModel.prototype.defaults, ModalModel.prototype.defaults);

		return forgotPasswordModel;
	}
);
