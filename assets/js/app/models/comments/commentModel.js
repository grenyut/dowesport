define(['app/models/baseModel'],
	function(BaseModel) {

		var commentModel = BaseModel.extend({
			//url: '/events/',
			//idAttribute: "id",

			defaults: function(){
				return _.extend({}, BaseModel.prototype.defaults, 
				{
					message: undefined,
					user: undefined
				});
			}
		});
		return commentModel;
	}
);
