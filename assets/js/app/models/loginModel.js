define(['app/models/modalModel'],
	function(ModalModel) {

		var loginModel = ModalModel.extend({
			url: '/login',

			defaults: {
				email: '',
				password: ''
			}
		});

		_.extend(loginModel.prototype.defaults, ModalModel.prototype.defaults);

		return loginModel;
	}
);
