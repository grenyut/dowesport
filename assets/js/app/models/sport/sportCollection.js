define(['backbone'],
    function(Backbone) {
        return Backbone.Collection.extend({
            searchTerm: undefined,
            url: function(){
                return "/sports/";
            },

            initialize: function() {
                //this.listenTo(this, 'sync', this.sportsFetched);
            },

            parse: function parse( response ) {
                var aux = response.filter(function isPreferred( element ) {
                    return element.userPref;
                });
                console.log('filtering array in parse method');
                console.log(aux);
                return aux;
            }
        });
    }
);
