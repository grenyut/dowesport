define(['app/models/baseModel'],
	function(BaseModel) {

		return BaseModel.extend({

			defaults: function(){
				return _.extend({}, BaseModel.prototype.defaults, 
				{
					id: undefined,
					name: undefined
				});
			},
			
			successCallback: function(model, response) {
				BaseModel.prototype.successCallback.call(this, model, response);
			},

            errorCallback: function(model, response, options) {
            	BaseModel.prototype.successCallback.call(this, model, response, options);
                if( response.status == 400 ) {
                		
                }            	
            }		

		});
	}
);
