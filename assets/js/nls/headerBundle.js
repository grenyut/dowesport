define({
    "root": {
      "initSession": "Iniciar sesión",
      "forgotPassword": "¿Has olvidado tu contraseña?",
      "rememberMe": "Recordarme",
      "register": "Regístrate",
      "alreadyMember": "¿Ya eres miembro de XXXXXX?",
      "withoutAccount": "¿No tienes una cuenta?",
      "name": "Nombre",
      "email": "Correo electrónico",
      "password": "Constraseña",
      "passConfirmation": "Confirmar contraseña", 
      "recoverPasswordInstruction": "Introduce la dirección de correo electrónico asociada a tu cuenta y te enviaremos un enlace para restablecer tu contraseña.", 
      "sendLink": "Enviar el enlace",
      "ERROR_FIELD": "Campo requerido"
    },
    "en-US": false
});