/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // '/': {
  //   view: 'homepage'
  // },

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

  'post /login': {
    controller: 'auth',
    action: 'login'
  },

  '/logout': {
    controller: 'auth',
    action: 'logout'
  },

  'post /profileimage': {
    controller: 'image',
    action: 'profile'
  },

  'post /event/:id/image': {
    controller: 'image',
    action: 'uploadEventImage'
  },

  'delete /event/:id/image/:id_image': {
    controller: 'image',
    action: 'deleteEventImage'
  },    

  'get /image/event/:id/:image': {
    controller: 'image',
    action: 'getImage'
  },  

  'get /image/user/:id/:image': {
    controller: 'image',
    action: 'getImage'
  },    

  // '/verify': {
  //   controller: 'user',
  //   action: 'verify'
  // },

  'get /user/show/:id': {
    controller: 'user',
    action: 'show'
  },

  'get /user/edit/:id': {
    controller: 'user',
    action: 'edit'
  },  

  'post /user': {
    controller: 'user',
    action: 'create'
  },  

  'put /user': {
    controller: 'user',
    action: 'update'
  },        

  '/removeaccount': {
    controller: 'user',
    action: 'removeaccount'
  }, 

  /////USER INFO
  'get /user/:id/interests': {
    controller: 'userInterests',
    action: 'edit'
  }, 

  // 'get /user/:id/interests/create': {
  //   controller: 'userInterests',
  //   action: 'create'
  // },

  'put /user/:id_user/interests/:id_sport': {
    controller: 'userInterests',
    action: 'create'
  }, 

  'delete /user/:id_user/interests/:id_sport': {
    controller: 'userInterests',
    action: 'delete'
  },
  /////END USER INFO

  'post /forgot_password': {
    controller: 'password',
    action: 'forgotPassword'
  },

  'get /reset_password': {
    controller: 'password',
    action: 'resetPassword'
  },

  'post /reset_password': {
    controller: 'password',
    action: 'newPassword'
  },



  'get /': {
    controller: 'views',
    action: 'home'
  },

  'get /password_modal': {
    controller: 'modals',
    action: 'getForgotPasswordModal'
  },

  'get /login_modal': {
    controller: 'modals',
    action: 'getLoginModal'
  },

  'get /register_modal': {
    controller: 'modals',
    action: 'getRegisterModal'
  },


  //RUTES PER ALS LLISTATS D'EVENTS / SEO GOOGLE????
  'get /events': {
    controller: 'event',
    action: 'list'
  },


  'get /event/:id/comments': {
    controller: 'eventInfo',
    action: 'eventComments'
  }, 

  'post /event/:id/comments': {
    controller: 'eventInfo',
    action: 'eventCreateComment'
  },  

  // 'get /event/:id/users': {
  //   controller: 'eventInfo',
  //   action: 'eventUsers'
  // }, 

  'get /event/edit/:id': {
    controller: 'event',
    action: 'edit'
  },  

  'get /event/new': {
    controller: 'event',
    action: 'new'
  },

  'get /event/:id/:title': {
    controller: 'event',
    action: 'show'
  },  

  'post /events': {
    controller: 'event',
    action: 'create'
  },

  'put /events': {
    controller: 'event',
    action: 'update'
  },  

  'get /events/:keyword1': {
    controller: 'event',
    action: 'list'
  },
  'get /events/:keyword1/:keyword2': {
    controller: 'event',
    action: 'list'
  },

  //  'get /events/:keyword1/:keyword2/:keyword3': {
  //    controller: 'event',
  //    action: 'list'
  //  },

  //
  'get /invitation/edit/:id': {
    controller: 'invitation',
    action: 'new'
  },

  //id is id_token (invitation table)
  // 'get /invitation/show/:id': {
  //   controller: 'invitation',
  //   action: 'show'
  // },    

  'get /invitation/accept/:token': {
    controller: 'invitation',
    action: 'acceptInvitation'
  },    

  'get /invitation/deny/:token': {
    controller: 'invitation',
    action: 'denyInvitation'
  }, 

  //ROOMS
  'get /room/invitation/:token': {
    controller: 'room',
    action: 'show'
  },    

  'get /room/event/:id': {
    controller: 'room',
    action: 'show'
  },

   //SPORT
  // 'get /sports': {
  //   controller: 'sport',
  //   action: 'search'
  // }, 
  //END SPORT

  //TODO COMMENT!!!!
  //TESTING ROUTES!!!!!!!!
  'get /testinsertnode/:email': {
    controller: 'testGraph',
    action: 'testInsertNode'
  },

  'get /testgetemails/:email': {
    controller: 'testGraph',
    action: 'testGetNodes'
  },

  'get /testsetrelationship/:destemail': {
    controller: 'testGraph',
    action: 'testSetRelationship'
  }

};
