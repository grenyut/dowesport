/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions (`true` allows public     *
  * access)                                                                  *
  *                                                                          *
  ***************************************************************************/

  // '*': true,

  /***************************************************************************
  *                                                                          *
  * Here's an example of mapping some policies to run before a controller    *
  * and its actions                                                          *
  *                                                                          *
  ***************************************************************************/
	// RabbitController: {

		// Apply the `false` policy as the default for all of RabbitController's actions
		// (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
		// '*': false,

		// For the action `nurture`, apply the 'isRabbitMother' policy
		// (this overrides `false` above)
		// nurture	: 'isRabbitMother',

		// Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
		// before letting any users feed our rabbits
		// feed : ['isNiceToAnimals', 'hasRabbitFood']
	// }


  // Default policy for all controllers and actions
  // (`true` allows public access)

  
  '*': false,

  views: {
    'home': "flash"
  },

  modals: {
    '*': true
  },

  user: {
    'new': "flash",
    //subscribe: ["flash", "authenticated"],
    'create': ["flash"],
    'show': ["flash"],
    'edit': ["flash", /*"authenticated", "userCanSeeProfile"*/],
    'update': ["flash", "authenticated", "userCanSeeProfile"],
    'index': ["flash", "authenticated", "admin"],
    'removeaccount': ["flash"]
  },

  userInterests: {
    'edit': ["flash"/*, "authenticated", "userCanSeeProfile"*/],
    'create': ["flash"/*, "authenticated", "userCanSeeProfile"*/],
    'delete': ["flash",/* "authenticated", "userCanSeeProfile"*/],
  },  

  event: {
    'new': ["flash"],
    'create': ["flash", "authenticated", "place.checkExistence"],
    'show': ["flash", "event.isAccessible"],
    'edit': ["flash", "authenticated", "event.checkOwnEvent"],
    'update': ["flash", /*"authenticated",*/ "place.checkExistence", "event.checkOwnEvent"],
    'destroy': ["flash", "authenticated", "event.checkOwnEvent"],
    'list': ["flash", "paginator", "event.checkEventListKeywords", "event.listEvent"],
    'index': ["flash", "authenticated", "admin"],
    'share': ["flash", /*"authenticated",*/ "event.checkOwnEvent"],
    'shareWith': ["flash", /*"authenticated",*/ "event.checkOwnEvent"]
  },

  eventInfo: {
    'eventComments': ["flash", "event.isAccessible"],
    'eventCreateComment': ["flash", "authenticated", "event.isAccessible"],
    //'eventUsers': ["flash"]
  },

  invitation: {
    'new': ["flash", /*"authenticated",*/ "event.checkOwnEvent"],
    'show': ["flash"],
        'create': ["flash" /*, "authenticated",*/], 
        'update': ["flash", 'authenticated'], 
        'acceptInvitation': ["flash", "authenticated"],
        'denyInvitation': ["flash", "authenticated"],
    //'shareWith': ["flash", /*"authenticated",*/ "event.checkOwnEvent"]
  },

  // sport: {
  //   'search': ["flash"/*, "authenticated"*/]
  // },  

  room: {
    'show': ["flash", "authenticated", "event.checkUserInvited"],
        //'create': ["flash", /*"authenticated",*/],    
    //'shareWith': ["flash", /*"authenticated",*/ "event.checkOwnEvent"]
  },  

  auth: {
    '*': true,
  },

  image: {
    'profile': ["authenticated", /*"userCanSeeProfile",*/ "createUserAvatar"],
    'uploadEventImage': ["authenticated", "event.checkOwnEvent", "event.uploadImage"],
    'deleteEventImage': ["authenticated", "event.checkOwnEvent"],
    'getImage': true
  },

  password: {
    'resetPassword': "flash",
    'forgotPassword': true,
    'newPassword': ["flash", "authenticated", "userCanSeeProfile", "checkAndSetNewPassword"]
  },


  //TODO COMMENT BEFORE DEPLOY IN PRODUCTION
  //TEST
  testGraph: {
    'testInsertNode': true,
    'testGetNodes': true,
    'testSetRelationship': true
  }

};
